<?php

namespace frontend\components\MenuWidget;

use frontend\components\MenuWidget\models\Item;
use yii\bootstrap\Widget;

/**
 * Class Menu
 * @package frontend\components\MenuWidget
 *
 * @property Item[] $items
 */

class Menu extends Widget
{
    public $items;

    private $_items;

    public function init()
    {
        foreach ($this->items as $item) {
            if (isset($item["visible"]) && $item["visible"] == false)
                continue;

            $this->_items[] = new Item($item);
        }
    }

    public function run()
    {
        return $this->render('view', ['items' => $this->_items]);
    }
}