<div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark m-aside-menu--dropdown " data-menu-vertical="true" m-menu-dropdown="1" m-menu-scrollable="0" m-menu-dropdown-timeout="500">
    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
        <?foreach ($items as $item):?>
            <?if($item->visible):?>
                <li class="m-menu__item <?=$item->classText?>" aria-haspopup="true" <?=$item->togle?>><a href="<?=$item->urlText?>" class="<?=$item->aClass?>">
                        <span class="m-menu__item-here"></span><i class="m-menu__link-icon <?=$item->icon?>"></i><span class="m-menu__link-text"><?=$item->name?></span></a>
                    <?if ($item->items):?>
                    <div class="m-menu__submenu "><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <?foreach ($item->items as $s_item):?>
                                <li class="m-menu__item " aria-haspopup="true">
                                    <a href="<?=$s_item->url?>" class="m-menu__link ">
                                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i><span class="m-menu__link-text"><?=$s_item->name?></span>
                                    </a>
                                </li>
                            <?endforeach;?>
                        </ul>
                        </div>
                    <?endif;?>
                </li>
            <?endif;?>
        <?endforeach;?>
    </ul>
</div>