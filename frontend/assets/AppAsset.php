<?php

namespace frontend\assets;


use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        //'assets2/vendors/base/vendors.bundle.css',
        //'assets2/demo/demo3/base/style.bundle.css',
        'css/site.css',
    ];
    public $js = [
        //'assets2/vendors/base/vendors.bundle.js',
        'assets2/demo/demo3/base/scripts.bundle.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
