<?php


/* @var $this \yii\web\View */

use kartik\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $model \frontend\models\products\ProductGroups|\yii\db\ActiveRecord */

$this->registerJs(
    '$("document").ready(function(){
            $("#pjax-add-product").on("pjax:end", function() {
                $.pjax.reload({container:"#pjax-grid-products"});
            });
    });'
);


?>

<div class="gridview-editable">
    <?Pjax::begin(['id' => 'pjax-add-product'])?>
        <? $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
            <?if (Yii::$app->user->can('products-create', ['model' => $model])):?>
                <?=Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Добавить', ['class' => 'btn btn-success', 'name' => 'create-product'])?>
            <?endif;?>
        <? ActiveForm::end();?>
    <?Pjax::end()?>

    <?=\kartik\grid\GridView::widget([
        'dataProvider' => $model->productsDataProvider,
        'columns' => [
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'nameText',
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/products/editrecord']],
                ],
                'readonly' => function($model){return !Yii::$app->user->can('products-update', ['model' => $model]);}
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'price',
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/products/editrecord']],
                ],
                'readonly' => function($model){return !Yii::$app->user->can('products-update', ['model' => $model]);}
            ],
            [
                'class' => \kartik\grid\ActionColumn::class,
                'template' => '{delete}',
                'controller' => 'products',
                'buttons' => [
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-pjax' => 'pjax-container',//pjax
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post'
                        ]);
                    },
                ],
                'visibleButtons' => [
                    'delete' => function($model){return Yii::$app->user->can('products-delete', ['model' => $model]);}
                ]
            ]
        ],
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => [
                'id' => 'pjax-grid-products'
            ]
        ],
        'panelBeforeTemplate' => '{before}'
    ])?>

</div>
