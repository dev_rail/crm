<?

use kartik\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;

$this->registerJs(
    '$("document").ready(function(){
            $("#pjax-add-item").on("pjax:end", function() {
                $.pjax.reload({container:"#pjax-grid-items"});
            });
            
            $("#pjax-grid-items").on("pjax:end", function(){
                $.pjax.reload({container:"#saleView"});
            });
    });'
);
?>

<h3>Позиции продажи</h3>

<div class="gridview-editable">

    <?Pjax::begin(['id' => 'pjax-add-item'])?>
        <? $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
            <?if (Yii::$app->user->can('saleitems-create')):?>
                <?=Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Добавить', ['class' => 'btn btn-success', 'name' => 'create-item'])?>
            <?endif;?>
        <? ActiveForm::end();?>
    <?Pjax::end()?>

    <?=\kartik\grid\GridView::widget([
        'dataProvider' => $model->itemsDataProvider,
        'columns' => [
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'product_id',
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/sale-items/editrecord']],
                    'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                    'options' => [
                        'data' => \frontend\models\products\Products::getAllArrayList()
                    ]
                ],
                'value' => function($model){
                    return \yii\helpers\ArrayHelper::getValue($model, 'product.name');
                },
                'refreshGrid' => true,
                'readonly' => !Yii::$app->user->can('saleitems-update')
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'count',
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/sale-items/editrecord']],
                ],
                'refreshGrid' => true,
                'readonly' => !Yii::$app->user->can('saleitems-update')
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'price',
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/sale-items/editrecord']],
                ],
                'refreshGrid' => true,
                'readonly' => !Yii::$app->user->can('saleitems-update')
            ],
            [
                'attribute' => 'sum'
            ],
            [
                'class' => \kartik\grid\ActionColumn::class,
                'template' => '{delete}',
                'controller' => 'sale-items',
                'buttons' => [
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-pjax' => 'pjax-container',//pjax
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post'
                        ]);
                    },
                ],
                'visibleButtons' => [
                    'delete' => Yii::$app->user->can('saleitems-delete')
                ]
            ]
        ],
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => [
                'id' => 'pjax-grid-items'
            ]
        ],
        'panelBeforeTemplate' => '{before}'
    ])?>

</div>
