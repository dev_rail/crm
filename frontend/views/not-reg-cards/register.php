<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\models\partners\NotRegCards */

$this->title = 'Регистрация новой карты';
?>

<div>
    <h2><?=\yii\helpers\Html::encode($this->title)?></h2>

    <h3>UID карты: <?=\yii\helpers\Html::encode($model->uid)?></h3>

    <?$form = \yii\widgets\ActiveForm::begin()?>
        <?=$form->field($model, 'partner_id')->widget(\kartik\select2\Select2::class, [
            'data' => \frontend\models\partners\Partners::getAllArrayList()
        ])?>

        <?=$form->field($model, "number")->textInput(['type' => 'number'])?>

        <?=$form->field($model, "name")?>

        <?=\yii\helpers\Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'name' => 'saveBtn'])?>
    <? \yii\widgets\ActiveForm::end()?>
</div>


