<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\models\Users|\yii\db\ActiveRecord */

$this->registerJs(
    '$("document").ready(function(){
            $("#pjax-add-partner").on("pjax:end", function() {
                $.pjax.reload({container:"#pjax-grid-partners"});
            });
    });'
);

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\Pjax; ?>

<h3>Привязанные контрагенты</h3>

<div class="gridview-editable">

    <?Pjax::begin(['id' => 'pjax-add-partner'])?>
    <? $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
    <?=Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Добавить', ['class' => 'btn btn-success', 'name' => 'add-partner'])?>
    <? ActiveForm::end();?>
    <?Pjax::end()?>

    <?=\kartik\grid\GridView::widget([
        'dataProvider' => $model->partnersDataProvider,
        'columns' => [
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'partner_id',
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/user-partners/editrecord']],
                    'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                    'options' => [
                        'data' => \frontend\models\partners\Partners::getAllArrayList()
                    ]
                ],
                'value' => function($data){
                    return \yii\helpers\ArrayHelper::getValue($data, "partner.name");
                }
            ],
            [
                'class' => \kartik\grid\ActionColumn::class,
                'template' => '{delete}',
                'controller' => 'user-partners',
                'buttons' => [
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-pjax' => 'pjax-container',//pjax
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post'
                        ]);
                    },
                ]
            ]
        ],
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => [
                'id' => 'pjax-grid-partners'
            ]
        ],
        'panelBeforeTemplate' => '{before}'
    ])?>

</div>

