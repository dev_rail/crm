<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Группы товаров и услуг';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-groups-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?if (Yii::$app->user->can('productgroups-create')):?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'nameText',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function($model){
                        return Yii::$app->user->can('productgroups-update', ['model' => $model]);
                    },
                    'view' => Yii::$app->user->can('productgroups-view'),
                    'delete' => function($model){
                        return Yii::$app->user->can('productgroups-delete', ['model' => $model]);
                    }
                ]
            ],
        ],
    ]); ?>
</div>
