<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\products\ProductGroups */

$this->title = $model->nameText;
$this->params['breadcrumbs'] = $model->breadcrumbs;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-groups-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?if (Yii::$app->user->can('productgroups-update', ['model' => $model])):?>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('productgroups-delete', ['model' => $model])):?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent.name',
            'nameText',
        ],
    ]) ?>

    <hr>

    <?=\kartik\tabs\TabsX::widget([
            'position' => \kartik\tabs\TabsX::ALIGN_CENTER,
            'items' => [
                [
                    'label' => 'Дочерние категории',
                    'content' => $this->render('gridview', ['model' => $model])
                ],
                [
                    'label' => 'Товары и услуги',
                    'content' => $this->render('@app/views/products/gridview', ['model' => $model]),
                    'visible' => Yii::$app->user->can('products-index')
                ]
            ]
    ])?>

</div>
