<?php

/* @var $this \yii\web\View */

use kartik\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $model \frontend\models\products\ProductGroups|\yii\db\ActiveRecord */

$this->registerJs(
    '$("document").ready(function(){
            $("#pjax-add-group").on("pjax:end", function() {
                $.pjax.reload({container:"#pjax-grid-groups"});
            });
    });'
);

?>

<div class="gridview-editable">
<?if (Yii::$app->user->can('productgroups-create', ['model' => $model])):?>
    <?Pjax::begin(['id' => 'pjax-add-group'])?>
        <? $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
            <?=Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Добавить', ['class' => 'btn btn-success', 'name' => 'create-group'])?>
        <? ActiveForm::end();?>
    <?Pjax::end()?>
<?endif;?>

<?=\kartik\grid\GridView::widget([
    'dataProvider' => $model->childrenCategoryDataProvider,
    'columns' => [
        [
            'class' => 'kartik\grid\EditableColumn',
            'attribute' => 'nameText',
            'editableOptions'=> [
                'formOptions' => ['action' => ['/product-groups/editrecord']],
            ],
            'readonly' => function($model){return !Yii::$app->user->can('productgroups-update', ['model' => $model]);}
        ],
        [
            'class' => \kartik\grid\ActionColumn::class,
            'template' => '{delete} {view}',
            'buttons' => [
                'delete' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('yii', 'Delete'),
                        'aria-label' => Yii::t('yii', 'Delete'),
                        'data-pjax' => 'pjax-container',//pjax
                        'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                        'data-method' => 'post'
                    ]);
                },
            ],
            'visibleButtons' => [
                'delete' => function($model)
                {
                    if ($model->hasProduct() or !Yii::$app->user->can('productgroups-delete', ['model' => $model]))
                        return false;

                    return true;
                },
                'view' => Yii::$app->user->can('productgroups-view')
            ]
        ]
    ],
    'pjax'=>true,
    'pjaxSettings'=>[
        'neverTimeout'=>true,
        'options' => [
            'id' => 'pjax-grid-groups'
        ]
    ],
    'panelBeforeTemplate' => '{before}'
])?>

</div>
