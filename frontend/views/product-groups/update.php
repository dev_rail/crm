<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\products\ProductGroups */

$this->title = 'Изменение группы товаров и услуг: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Группы товаров и услуг', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="product-groups-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
