<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\inpayments\Inpayments */

$this->title = 'Изменение оплаты: ' . $model->partner->name;
$this->params['breadcrumbs'][] = ['label' => 'Оплаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="inpayments-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
