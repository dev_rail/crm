<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\inpayments\Inpayments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="inpayments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dateText')->widget(\kartik\widgets\DateTimePicker::class, [
            'pluginOptions' => [
                'format' => 'dd.mm.yyyy hh:ii:ss'
            ]
    ]) ?>

    <?= $form->field($model, 'partner_id')->widget(\kartik\select2\Select2::class, [
            'data' => \frontend\models\partners\Partners::getAllArrayList()
    ]) ?>

    <?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
