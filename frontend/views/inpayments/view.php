<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\inpayments\Inpayments */

$this->title = "Оплата от ".$model->partner->name;
$this->params['breadcrumbs'][] = ['label' => 'Оплаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inpayments-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?if(Yii::$app->user->can('inpayments-update')):?>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('inpayments-delete')):?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'partner.name',
            'dateText',
            'sum',
            'description',
        ],
    ]) ?>

</div>
