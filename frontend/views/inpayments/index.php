<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оплаты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inpayments-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?if(Yii::$app->user->can('inpayments-create')):?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'dateText',
            'partner.name',
            'sum',
            'description',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => Yii::$app->user->can('inpayments-update'),
                    'view' => Yii::$app->user->can('inpayments-view'),
                    'delete' => Yii::$app->user->can('inpayments-delete'),
                ]
            ],
        ],
    ]); ?>
</div>
