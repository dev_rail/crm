<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\inpayments\Inpayments */

$this->title = 'Новая оплата';
$this->params['breadcrumbs'][] = ['label' => 'Оплаты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inpayments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
