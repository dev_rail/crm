<?php

use kartik\widgets\ActiveForm;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model frontend\models\sales\Sales */

$this->title = "Продажа ".$model->partner->name." от ".$model->dateText;
$this->params['breadcrumbs'][] = ['label' => 'Продажи', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sales-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?if (Yii::$app->user->can('sales-update')):?>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('sales-delete')):?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?endif;?>
    </p>
    <?php Pjax::begin(['id' => 'saleView']); ?>
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'dateText',
                'object.nameText',
                'partner.name',
                'payText',
                'sum'
            ],
        ]) ?>


        <?if($model->saleCard):?>
            <hr>

            <h3>Карта</h3>

            <?= DetailView::widget([
                'model' => $model->saleCard,
                'attributes' => [
                    'card.name',
                    'card.number',
                    [
                        'attribute' => 'bonus',
                        'visible' => ($model->type_pay != \frontend\models\sales\Sales::TYPE_PAY_BONUS)
                    ],
                    [
                        'attribute' => 'bonus_percent',
                        'visible' => ($model->type_pay != \frontend\models\sales\Sales::TYPE_PAY_BONUS)
                    ],
                ],
            ]) ?>

        <?endif;?>
    <?php Pjax::end(); ?>

    <hr>
    <?if (Yii::$app->user->can('saleitems-index')):?>
        <?=$this->render('@frontend/views/sale-items/index', ['model' => $model])?>
    <?endif;?>
</div>
