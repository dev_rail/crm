<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Терминалы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="terminals-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?if (Yii::$app->user->can('terminals-create')):?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'object.name',
            'imei',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => Yii::$app->user->can('terminals-update'),
                    'view' => Yii::$app->user->can('terminals-view'),
                    'delete' => Yii::$app->user->can('terminals-delete')
                ]
            ],
        ],
    ]); ?>
</div>
