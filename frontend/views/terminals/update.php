<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\objects\Terminals */

$this->title = 'Изменение терминала ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Терминалы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Изменение';
?>
<div class="terminals-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
