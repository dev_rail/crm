<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\objects\Terminals */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="terminals-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'object_id')->widget(\kartik\widgets\Select2::class, ['data' => \frontend\models\objects\Objects::getAllArrayList()]) ?>

    <?= $form->field($model, 'imei')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
