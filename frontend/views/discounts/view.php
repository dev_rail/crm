<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\discounts\Discounts */

$this->title = $model->product->nameText;
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['/partners/index']];
$this->params['breadcrumbs'][] = ['label' => $model->partner->name, 'url' => ['/partners/view', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = ['label' => 'Скидочная система', 'url' => ['index', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discounts-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?if (Yii::$app->user->can('discounts-update')):?>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('discounts-delete')):?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'product.nameText',
            'typeText',
            'amount'
        ],
    ]) ?>

    <hr>

    <?if (Yii::$app->user->can('discountcards-index')
        or Yii::$app->user->can('discountobjects-index')):?>
        <?=\kartik\tabs\TabsX::widget([
            'position' => \kartik\tabs\TabsX::ALIGN_CENTER,
            'items' => [
                [
                    'label' => 'Карты',
                    'content' => $this->render("@frontend/views/discount-cards/index",
                        [
                            'dataProvider' => $model->discountCardsDataProvider,
                            'model' => $model
                        ]),
                    'visible' => Yii::$app->user->can('discountcards-index')
                ],
                [
                    'label' => 'Объекты',
                    'content' => $this->render("@frontend/views/discount-objects/index",
                        [
                            'dataProvider' => $model->discountObjectsDataProvider,
                            'model' => $model
                        ]),
                    'visible' => Yii::$app->user->can('discountobjects-index')
                ]
            ]
        ])?>
    <?endif;?>
</div>
