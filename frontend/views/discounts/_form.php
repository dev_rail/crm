<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\discounts\Discounts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="discounts-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'product_id')->widget(\kartik\select2\Select2::class, [
         'data' => \frontend\models\products\Products::getAllArrayList()
    ]) ?>

    <?= $form->field($model, 'type')->widget(\kartik\select2\Select2::class, [
         'data' => \frontend\models\discounts\Discounts::TYPES
    ]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
