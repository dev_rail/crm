<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\discounts\Discounts */

$this->title = 'Создание скидки';
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['/partners/index']];
$this->params['breadcrumbs'][] = ['label' => $model->partner->name, 'url' => ['/partners/view', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = ['label' => 'Скидочная система', 'url' => ['index', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discounts-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
