<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $partner \frontend\models\partners\Partners */

$this->title = 'Скидочная система '.$partner->name;
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['/partners/index']];
$this->params['breadcrumbs'][] = ['label' => $partner->name, 'url' => ['/partners/view', 'id' => $partner->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="discounts-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?if (Yii::$app->user->can('discounts-create')):?>
        <p>
            <?= Html::a('Добавить', ['create', 'id' => $partner->id], ['class' => 'btn btn-success']) ?>
        </p>
    <?endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'product.nameText',
            'typeText',
            'amount',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
