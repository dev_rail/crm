<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model frontend\models\partners\Cards */

$this->title = 'Изменение карты: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['/partners/index']];
$this->params['breadcrumbs'][] = ['label' => $model->partner->name, 'url' => ['/partners/view', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = ['label' => 'Карты', 'url' => ['index', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cards-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
