<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Карты '.$partner->name;
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['/partners/index']];
$this->params['breadcrumbs'][] = ['label' => $partner->name, 'url' => ['/partners/view', 'id' => $partner->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cards-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?if (Yii::$app->user->can('cards-create')):?>
        <p>
            <?= Html::a('Добавить', ['create', 'id' => $partner->id], ['class' => 'btn btn-success']) ?>
        </p>
    <?endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'number',
            'uid',
            'name',
            'periodTypeText',
            'limitTypeText',
            'limit_value',
            'bonus_balance',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => Yii::$app->user->can('cards-update'),
                    'view' => Yii::$app->user->can('cards-view'),
                    'delete' => Yii::$app->user->can('cards-delete'),
                ]
            ],
        ],
    ]); ?>
</div>
