<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\partners\Cards */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['/partners/index']];
$this->params['breadcrumbs'][] = ['label' => $model->partner->name, 'url' => ['/partners/view', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = ['label' => 'Карты', 'url' => ['index', 'id' => $model->partner_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cards-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?if (Yii::$app->user->can('cards-update')):?>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('cards-delete')):?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'partner.name',
            'number',
            'uid',
            'name',
            'periodTypeText',
            'limitTypeText',
            'limit_value'
        ],
    ]) ?>

</div>
