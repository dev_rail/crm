<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\partners\Cards */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cards-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'uid')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'period_type')->widget(\kartik\select2\Select2::class, [
        'data' => \frontend\models\partners\Cards::getAllTypePeriods()
    ])?>

    <?=$form->field($model, 'limit_type')->widget(\kartik\select2\Select2::class, [
        'data' => \frontend\models\partners\Cards::getAllTypeLimits()
    ])?>

    <?=$form->field($model, "limit_value")?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
