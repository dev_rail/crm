<?php


/* @var $this \yii\web\View */
$this->title = 'Отчет по реализации товаров и услуг';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <h1><?=$this->title?></h1>

    <hr>
    <?$form = \yii\bootstrap\ActiveForm::begin()?>
        <div class="row">
            <div class="col-lg-2">
                <?=$form->field($model, 'date_start')->widget(\kartik\widgets\DateTimePicker::class, [
                    'pluginOptions' => [
                        'orientation' => 'bottom',
                        'format' => 'dd.mm.yyyy hh:ii'
                    ],
                    'options' => [
                        'placeholder' => 'Дата начала'
                    ]
                ])->label(false)?>
            </div>
            <div class="col-lg-2">
                <?=$form->field($model, 'date_end')->widget(\kartik\widgets\DateTimePicker::class, [
                    'pluginOptions' => [
                        'orientation' => 'bottom',
                        'format' => 'dd.mm.yyyy hh:ii'
                    ],
                    'options' => [
                        'placeholder' => 'Дата конца'
                    ]
                ])->label(false)?>
            </div>
            <div class="col-lg-2">
                <?=$form->field($model, 'object')->widget(\kartik\select2\Select2::class, [
                    'data' => \frontend\models\objects\Objects::getAllArrayList(true),
                    'options' => [
                        'placeholder' => 'Объект'
                    ]
                ])->label(false)?>
            </div>
            <?if (!$model->is_pay_type):?>
                <div class="col-lg-2">
                    <?=$form->field($model, 'type_pay')->widget(\kartik\select2\Select2::class, [
                        'data' => \frontend\models\sales\Sales::TYPES_PAY,
                        'options' => [
                            'placeholder' => 'Тип оплаты'
                        ]
                    ])->label(false)?>
                </div>
            <?endif;?>
            <div class="col-lg-2">
                <?=$form->field($model, 'is_pay_type')->checkbox()?>
            </div>
            <div class="col-lg-2">
                <?=\yii\helpers\Html::submitButton('Применить', ['class' => 'btn btn-success'])?>
            </div>
        </div>
    <? \yii\bootstrap\ActiveForm::end()?>

    <hr>

    <?=\kartik\grid\GridView::widget([
        'dataProvider' => $model->search(),
        'columns' => $model->visibleColumns,
        'showFooter' => true,
        'showPageSummary' => true,
        'panelFooterTemplate' => 'Итого'
    ])?>
</div>
