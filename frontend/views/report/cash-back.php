<?php

/* @var $this \yii\web\View */
/* @var $model \frontend\models\reports\CashBackForm */
$this->title = 'Отчет по комиссии контрагента';
$this->params['breadcrumbs'][] = $this->title;
?>

<div>
    <h1><?=$this->title?></h1>

    <hr>
    <?$form = \yii\bootstrap\ActiveForm::begin()?>
    <div class="row">
        <div class="col-lg-2">
            <?=$form->field($model, 'date_start')->widget(\kartik\widgets\DateTimePicker::class, [
                'pluginOptions' => [
                    'orientation' => 'bottom',
                    'format' => 'dd.mm.yyyy hh:ii'
                ],
                'options' => [
                    'placeholder' => 'Дата начала'
                ]
            ])->label(false)?>
        </div>
        <div class="col-lg-2">
            <?=$form->field($model, 'date_end')->widget(\kartik\widgets\DateTimePicker::class, [
                'pluginOptions' => [
                    'orientation' => 'bottom',
                    'format' => 'dd.mm.yyyy hh:ii'
                ],
                'options' => [
                    'placeholder' => 'Дата конца'
                ]
            ])->label(false)?>
        </div>
        <div class="col-lg-2">
            <?=$form->field($model, 'object')->widget(\kartik\select2\Select2::class, [
                'data' => \frontend\models\objects\Objects::getAllArrayList(true),
                'options' => [
                    'placeholder' => 'Объект'
                ]
            ])->label(false)?>
        </div>
        <div class="col-lg-2">
            <?=$form->field($model, 'partner')->widget(\kartik\select2\Select2::class, [
                'data' => \frontend\models\partners\Partners::getAllArrayList(true),
                'options' => [
                    'placeholder' => 'Контрагент'
                ]
            ])->label(false)?>
        </div>
        <div class="col-lg-2">
            <?=\yii\helpers\Html::submitButton('Применить', ['class' => 'btn btn-success'])?>
        </div>
    </div>
    <? \yii\bootstrap\ActiveForm::end()?>
    <hr>

    <?=\kartik\grid\GridView::widget([
        'dataProvider' => $model->search(),
        'columns' => [
            'sale.dateText',
            'sale.object.nameText',
            'sale.partner.name',
            'product.nameText',
            [
                'attribute' => 'price',
                'label' => 'Цена контрагента',
                'class' => \kartik\grid\DataColumn::class,
                'pageSummary' => 'Итого: '
            ],
            [
                'attribute' => 'saleItemCashBack.price',
                'label' => 'Цена реализации',
                'class' => \kartik\grid\DataColumn::class
            ],
            [
                'attribute' => 'count',
                'class' => \kartik\grid\DataColumn::class,
                'pageSummary' => true
            ],
            [
                'class' => \kartik\grid\DataColumn::class,
                'attribute' => 'sum',
                'label' => 'Сумма контрагента',
                'pageSummary' => true
            ],
            [
                'class' => \kartik\grid\DataColumn::class,
                'label' => 'Сумма реализации',
                'value' => function($model){
                    return $model->count * $model->saleItemCashBack->price;
                },
                'pageSummary' => true
            ],
            [
                'class' => \kartik\grid\DataColumn::class,
                'label' => 'Сумма комиссии',
                'attribute' => 'saleItemCashBack.sum',
                'pageSummary' => true
            ]
        ],
        'showPageSummary' => true
    ])?>
</div>

