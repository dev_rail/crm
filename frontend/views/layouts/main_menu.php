<!-- BEGIN: Aside Menu -->
<?=\frontend\components\MenuWidget\Menu::widget([
    'items' => [
        [
            'icon' => 'flaticon-network',
            'name' => 'Контрагенты',
            'items' => [
                [
                    'name' => 'Контрагенты',
                    'url' => \yii\helpers\Url::to(['/partners/index']),
                    'visible' => Yii::$app->user->can('partners-index')
                ],
                [
                    'name' => 'Незарегестрированные карты',
                    'url' => \yii\helpers\Url::to(['/not-reg-cards/index']),
                    'visible' => Yii::$app->user->can('notregcards-index')
                ]
            ]
        ],
        [
            'icon' => 'flaticon-coins',
            'name' => 'Оплаты',
            'url' => \yii\helpers\Url::to(['/inpayments/index']),
            'visible' => Yii::$app->user->can('inpayments-index')
        ],
        [
            'icon' => 'flaticon-cart',
            'name' => 'Продажи',
            'url' => \yii\helpers\Url::to(['/sales/index']),
            'visible' => Yii::$app->user->can('sales-index')
        ],
        [
            'icon' => 'flaticon-price-tag',
            'name' => 'Товары и услуги',
            'url' => \yii\helpers\Url::to(['/product-groups/index']),
            'visible' => Yii::$app->user->can('productgroups-index')
        ],
        [
            'icon' => 'flaticon-map-location',
            'name' => 'Объекты',
            'items' => [
                [
                    'name' => 'Объекты',
                    'url' => \yii\helpers\Url::to(['/objects/index']),
                    'visible' => Yii::$app->user->can('objects-index')
                ],
                [
                    'name' => 'Терминалы',
                    'url' => \yii\helpers\Url::to(['/terminals/index']),
                    'visible' => Yii::$app->user->can('terminals-index')
                ]
            ]
        ],
        [
            'icon' => 'flaticon-line-graph',
            'name' => 'Отчеты',
            'items' => [
                [
                    'name' => 'Реализация товаров и услуг',
                    'url' => \yii\helpers\Url::to(['/report/sales-goods']),
                    'visible' => Yii::$app->user->can('report-salesgoods')
                ],
                [
                    'name' => 'Продажа товаров и услуг',
                    'url' => \yii\helpers\Url::to(['/report/sales']),
                    'visible' => Yii::$app->user->can('report-sales')
                ],
                [
                    'name' => 'Комиссия контрагента',
                    'url' => \yii\helpers\Url::to(['/report/cash-back']),
                    'visible' => Yii::$app->user->can('report-cashback')
                ]
            ]
        ],
        [
            'icon' => 'flaticon-user',
            'name' => 'Пользователи',
            'items' => [
                [
                    'name' => 'Пользователи',
                    'url' => \yii\helpers\Url::to(['/user/index']),
                    'visible' => Yii::$app->user->can('user-index')
                ]
            ]
        ],
    ]
])?>
<!-- END: Aside Menu -->