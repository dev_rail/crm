<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Объекты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="objects-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?if (Yii::$app->user->can('objects-create')):?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'nameText',
            'address',
            'position',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => function($model){return Yii::$app->user->can('objects-update', ['model' => $model]);},
                    'view' => Yii::$app->user->can('objects-view'),
                    'delete' => function($model){return Yii::$app->user->can('objects-delete', ['model' => $model]);},
                ]
            ],
        ],
    ]); ?>
</div>
