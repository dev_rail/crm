<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\objects\Objects */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="objects-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model, 'address')->widget(\kalyabin\maplocation\SelectMapLocationWidget::class, [
        'googleMapApiKey' => Yii::$app->params['google-map']['key'],
        'attributeLatitude' => 'lat',
        'attributeLongitude' => 'lng',
        'draggable' => true,
    ])?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
