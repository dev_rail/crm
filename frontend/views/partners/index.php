<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Контрагенты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partners-index">


    <h1><?= Html::encode($this->title) ?></h1>


    <?if (Yii::$app->user->can('partners-create')):?>
        <p>
            <?= Html::a('Добавить', ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?endif;?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'name',
            'inn',
            'address',
            'balance',
            'limit',
            'enabledBalanceSum',
            'countCards',
            [
                'class' => 'yii\grid\ActionColumn',
                'visibleButtons' => [
                    'update' => Yii::$app->user->can('partners-update'),
                    'view' => Yii::$app->user->can('partners-view'),
                    'delete' => Yii::$app->user->can('partners-delete'),
                ]
            ],
        ],
    ]); ?>
</div>
