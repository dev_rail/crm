<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model frontend\models\partners\Partners */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partners-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?if (Yii::$app->user->can('partners-update')):?>
            <?= Html::a('Изменить', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('partners-delete')):?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('cards-index')):?>
            <?= Html::a('Карты', ['/cards/index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?endif;?>
        <?if (Yii::$app->user->can('discounts-index')):?>
            <?= Html::a('Скидочная система', ['/discounts/index', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?endif;?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'name',
            'full_name',
            'inn',
            'address',
            'balance',
            'limit',
            'enabledBalanceSum',
            'bonus'
        ],
    ]) ?>

</div>
