<?php

/* @var $this \yii\web\View */

use kartik\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Html;

/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->registerJs(
    '$("document").ready(function(){
            $("#pjax-add-discount-card").on("pjax:end", function() {
                $.pjax.reload({container:"#pjax-grid-discount-cards"});
            });
    });'
);
?>

<div class="gridview-editable">
    <?Pjax::begin(['id' => 'pjax-add-discount-card'])?>
        <? $form = ActiveForm::begin(['options' => ['data-pjax' => true]]);?>
            <?if (Yii::$app->user->can('discountcards-create')):?>
                <?=Html::submitButton('<i class="glyphicon glyphicon-plus"></i> Добавить', ['class' => 'btn btn-success', 'name' => 'create-discount-card', 'value' => 1])?>
            <?endif;?>
        <? ActiveForm::end();?>
    <?Pjax::end()?>

    <?=\kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'card_id',
                'value' => function($data){
                    return \yii\helpers\ArrayHelper::getValue($data, "card.name");
                },
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/discount-cards/editrecord']],
                    'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                    'options' => [
                        'data' => $model->allCards
                    ]
                ],
                'readonly' => !Yii::$app->user->can('discountcards-update')
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'type',
                'value' => function($data){
                    return \yii\helpers\ArrayHelper::getValue($data, "typeText");
                },
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/discount-cards/editrecord']],
                    'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
                    'options' => [
                        'data' => \frontend\models\discounts\Discounts::TYPES
                    ]
                ],
                'readonly' => !Yii::$app->user->can('discountcards-update')
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'amount',
                'editableOptions'=> [
                    'formOptions' => ['action' => ['/discount-cards/editrecord']],
                ],
                'readonly' => !Yii::$app->user->can('discountcards-update')
            ],
            [
                'class' => \kartik\grid\ActionColumn::class,
                'template' => '{delete}',
                'controller' => 'discount-cards',
                'buttons' => [
                    'delete' => function ($url) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'aria-label' => Yii::t('yii', 'Delete'),
                            'data-pjax' => 'pjax-container',//pjax
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'POST'
                        ]);
                    },
                ],
                'visibleButtons' => [
                    'delete' => function($model)
                    {
                        if (!Yii::$app->user->can('discountcards-delete'))
                            return false;

                        return true;
                    }
                ]
            ]
        ],
        'pjax'=>true,
        'pjaxSettings'=>[
            'neverTimeout'=>true,
            'options' => [
                'id' => 'pjax-grid-discount-cards'
            ]
        ],
        'panelBeforeTemplate' => '{before}'
    ])?>
</div>