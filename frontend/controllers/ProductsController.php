<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 03.11.18
 * Time: 10:49
 */

namespace frontend\controllers;


use frontend\models\products\ProductGroups;
use frontend\models\products\Products;
use kartik\grid\EditableColumnAction;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class ProductsController extends BaseController
{
    public function behaviors()
    {
        $behaviors =  parent::behaviors();
        $behaviors["access"]["rules"] = ArrayHelper::merge([
            [
                'actions' => ['editrecord'],
                'allow' => true,
                'roles' => [$this->permissionUpdate],
            ]
        ], $behaviors["access"]["rules"]);

        return $behaviors;
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editrecord' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => Products::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {

                    return $model->$attribute;
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
                'errorOptions' => ['header' => ''],
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                'checkAccess' => function($action, $model) {return \Yii::$app->user->can('products-update', ['model' => $model]);}
            ]
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!\Yii::$app->user->can('products-delete', ['model' => $model]))
            throw new ForbiddenHttpException('Нет прав на выполнение данной операции!');

        $parent_id = $model->group_id;
        $model->delete();

        if (\Yii::$app->request->isAjax) {
            $model = ProductGroups::findOne($parent_id);
            return $this->renderAjax('gridview', ['model' => $model]);
        }
    }

    /**
     * Finds the ProductGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Products|null the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = Products::find()
            ->processing()
            ->where(['id' => $id])
            ->one();

        if ($model !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}