<?php

namespace frontend\controllers;

use Yii;
use frontend\models\partners\NotRegCards;
use yii\data\ActiveDataProvider;
use frontend\controllers\BaseController;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * NotRegCardsController implements the CRUD actions for NotRegCards model.
 */
class NotRegCardsController extends BaseController
{
    public function getPermissionRegister()
    {
        return $this->idText."-register";
    }

    public function getPermissions()
    {
        return [
            $this->permissionIndex => 'Просмотр',
            $this->permissionRegister => 'Регистрация',
            $this->permissionDelete => 'Удаление',
        ];
    }

    public function behaviors()
    {
        $behaviors =  parent::behaviors();
        $behaviors["access"]["rules"] = ArrayHelper::merge([
            [
                'actions' => ['register'],
                'allow' => true,
                'roles' => [$this->permissionRegister],
            ]
        ], $behaviors["access"]["rules"]);

        return $behaviors;
    }
    /**
     * Lists all NotRegCards models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => NotRegCards::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRegister($id)
    {
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();

        if (Yii::$app->request->isPost && isset($post['saveBtn']))
        {
            $model->load($post);

            if ($model->validate() && $model->registerCard())
                $this->redirect(['index']);
        }

        return $this->render('register', ['model' => $model]);
    }


    /**
     * Deletes an existing NotRegCards model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the NotRegCards model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return NotRegCards the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = NotRegCards::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
