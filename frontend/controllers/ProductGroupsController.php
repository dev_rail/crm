<?php

namespace frontend\controllers;

use frontend\models\partners\Partners;
use kartik\grid\EditableColumnAction;
use Yii;
use frontend\models\products\ProductGroups;
use yii\data\ActiveDataProvider;
use frontend\controllers\BaseController;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductGroupsController implements the CRUD actions for ProductGroups model.
 */
class ProductGroupsController extends BaseController
{
    public function behaviors()
    {
        $behaviors =  parent::behaviors();
        $behaviors["access"]["rules"] = ArrayHelper::merge([
            [
                'actions' => ['editrecord'],
                'allow' => true,
                'roles' => [$this->permissionUpdate],
            ]
        ], $behaviors["access"]["rules"]);

        return $behaviors;
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editrecord' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => ProductGroups::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {

                    return $model->$attribute;
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
                'errorOptions' => ['header' => ''],
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                'checkAccess' => function($action, $model) {
                    return Yii::$app->user->can('productgroups-update', ['model' => $model]);
                }
            ]
        ]);
    }

    /**
     * Lists all ProductGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ProductGroups::find()
                ->processing()
                ->andWhere(['parent_id' => null]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProductGroups model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if (isset($post['create-group']) && Yii::$app->user->can('productgroups-create', ['model' => $model]))
            $model->addCategory();

        if (isset($post['create-product']) && Yii::$app->user->can('products-create', ['model' => $model]))
            $model->addProduct();

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ProductGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProductGroups();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProductGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('productgroups-update', ['model' => $model]))
            throw new ForbiddenHttpException('Нет прав на выполнение данной операции!');

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProductGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (!Yii::$app->user->can('productgroups-delete', ['model' => $model]))
            throw new ForbiddenHttpException('Нет прав на выполнение данной операции!');

        $parent_id = $model->parent_id;
        if (Yii::$app->request->isAjax) {
            $model->delete();
            $parent = $this->findModel($parent_id);

            return $this->renderAjax('gridview', ['model' => $parent]);
        }

        $model->delete();

        if ($parent_id)
            return $this->redirect(['/product-groups/view', 'id' => $parent_id]);
        else
            return $this->redirect(['index']);
    }

    /**
     * Finds the ProductGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProductGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $model = ProductGroups::find()
            ->processing()
            ->where(['id' => $id])->one();

        if ($model!== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
