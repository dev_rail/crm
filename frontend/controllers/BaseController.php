<?php

namespace frontend\controllers;

use yii\bootstrap\Alert;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

class BaseController extends Controller
{
    public function getIdText()
    {
        return str_replace("-", "", $this->id);
    }

    public function getPermissionIndex()
    {
        return $this->idText."-index";
    }

    public function getPermissionUpdate()
    {
        return $this->idText."-update";
    }

    public function getPermissionView()
    {
        return $this->idText."-view";
    }

    public function getPermissionDelete()
    {
        return $this->idText."-delete";
    }

    public function getPermissionCreate()
    {
        return $this->idText."-create";
    }

    public function getPermissions()
    {
        return [
            $this->permissionIndex => 'Список',
            $this->permissionCreate => 'Создание',
            $this->permissionDelete => 'Удаление',
            $this->permissionView => 'Просмотр',
            $this->permissionUpdate => 'Изменение'
        ];
    }

    public function getPermissionsForRule()
    {
        $result = [];

        foreach ($this->permissions as $permission => $description)
            $result[] = $permission;

        return $result;
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['*'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => [$this->permissionIndex],
                    ],
                    [
                        'actions' => ['view'],
                        'allow' => true,
                        'roles' => [$this->permissionView],
                    ],
                    [
                        'actions' => ['update'],
                        'allow' => true,
                        'roles' => [$this->permissionUpdate],
                    ],
                    [
                        'actions' => ['delete'],
                        'allow' => true,
                        'roles' => [$this->permissionDelete],
                    ],
                    [
                        'actions' => ['create'],
                        'allow' => true,
                        'roles' => [$this->permissionCreate],
                    ],
                    [
                        'actions' => ['default'],
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post']
                ],
            ],
        ];
    }

    public function actionDefault()
    {
        $auth = \Yii::$app->authManager;
        $role = $auth->getRolesByUser(\Yii::$app->user->id);
        $role = current($role);

        $this->redirect($role->data["defaultRoute"]);
    }
}