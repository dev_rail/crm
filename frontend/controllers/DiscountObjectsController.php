<?php

namespace frontend\controllers;

use frontend\models\discounts\DiscountObjects;
use kartik\grid\EditableColumnAction;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class DiscountObjectsController extends BaseController
{
    /**
     * Action editrecord gets rule from rule update.
     * @return array
     */
    public function behaviors()
    {
        $behaviors =  parent::behaviors();
        $behaviors["access"]["rules"] = ArrayHelper::merge([
            [
                'actions' => ['editrecord'],
                'allow' => true,
                'roles' => [$this->permissionUpdate],
            ]
        ], $behaviors["access"]["rules"]);

        return $behaviors;
    }

    /**
     * Description all actions in one method.
     * @return array
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editrecord' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => DiscountObjects::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {
                    switch ($attribute) {
                        case "object_id":
                            return $model->object->nameText;
                        case "type":
                            return $model->typeText;
                    }

                    return $model->$attribute;
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
                'errorOptions' => ['header' => ''],
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ]
        ]);
    }

    /**
     * Deletes an existing DiscountObject model.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $discount = $model->discount;
        $model->delete();

        if (\Yii::$app->request->isAjax)
        {
            return $this->renderAjax('index', ['dataProvider' => $discount->discountObjectsDataProvider, 'model' => $discount]);
        }
    }

    /**
     * Finds the DiscountCards model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param $id
     * @return DiscountObjects|null
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        $model = DiscountObjects::findOne($id);

        if (!$model)
            throw new NotFoundHttpException('The requested page does not exist.');

        return $model;
    }
}