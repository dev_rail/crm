<?php


namespace frontend\controllers;


use frontend\models\partners\UserPartners;
use frontend\models\Users;
use kartik\grid\EditableColumnAction;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class UserPartnersController extends BaseController
{
    public function behaviors()
    {
        $behaviors =  parent::behaviors();
        $behaviors["access"]["rules"] = ArrayHelper::merge([
            [
                'actions' => ['editrecord'],
                'allow' => true,
                'roles' => [$this->permissionUpdate],
            ]
        ], $behaviors["access"]["rules"]);

        return $behaviors;
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editrecord' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => UserPartners::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {

                    switch ($attribute)
                    {
                        case "partner_id":
                            return $model->partner->name;
                    }

                    return $model->$attribute;
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
                'errorOptions' => ['header' => ''],
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ]
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $user_id = $model->user_id;
        $model->delete();

        if (\Yii::$app->request->isAjax) {
            $user = Users::findOne($user_id);
            return $this->renderAjax('index', ['model' => $user]);
        }
    }

    /**
     * Finds the UserPartners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserPartners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserPartners::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
