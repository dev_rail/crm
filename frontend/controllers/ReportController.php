<?php

namespace frontend\controllers;


use frontend\models\reports\CashBackForm;
use frontend\models\reports\SalesForm;
use frontend\models\reports\SalesGoodsForm;
use yii\helpers\ArrayHelper;

class ReportController extends BaseController
{
    public function getPermissionSales()
    {
        return $this->id."-sales";
    }

    public function getPermissionSalesGoods()
    {
        return $this->id."-salesgoods";
    }

    public function getPermissionCashBack()
    {
        return $this->id."-cashback";
    }

    public function getPermissions()
    {
        return [
            $this->permissionSales => 'Продажи',
            $this->permissionSalesGoods => 'Реализации',
            $this->permissionCashBack => 'Комиссия контрагента'
        ];
    }

    public function behaviors()
    {
        $behaviors =  parent::behaviors();
        $behaviors["access"]["rules"] = [
            [
                'actions' => ['sales'],
                'allow' => true,
                'roles' => [$this->permissionSales],
            ],
            [
                'actions' => ['sales-goods'],
                'allow' => true,
                'roles' => [$this->permissionSalesGoods],
            ],
            [
                'actions' => ['cash-back'],
                'allow' => true,
                'roles' => [$this->permissionCashBack],
            ],
        ];

        return $behaviors;
    }

    public function actionSales()
    {
        $model = new SalesForm();

        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->validate();
        }

        return $this->render('sales', ['model' => $model]);
    }

    public function actionSalesGoods()
    {
        $model = new SalesGoodsForm();

        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->validate();
        }

        return $this->render('sales-goods', ['model' => $model]);
    }

    public function actionCashBack()
    {
        $model = new CashBackForm();

        if (\Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $model->validate();
        }

        return $this->render('cash-back', ['model' => $model]);
    }
}