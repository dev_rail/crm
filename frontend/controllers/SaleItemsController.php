<?php


namespace frontend\controllers;


use frontend\models\sales\SaleItems;
use frontend\models\sales\Sales;
use kartik\grid\EditableColumnAction;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class SaleItemsController extends BaseController
{
    public function behaviors()
    {
        $behaviors =  parent::behaviors();
        $behaviors["access"]["rules"] = ArrayHelper::merge([
            [
                'actions' => ['editrecord'],
                'allow' => true,
                'roles' => [$this->permissionUpdate],
            ]
        ], $behaviors["access"]["rules"]);

        return $behaviors;
    }

    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editrecord' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => SaleItems::className(),
                'outputValue' => function ($model, $attribute, $key, $index) {

                    switch ($attribute)
                    {
                        case "product_id":
                            return $model->product->name;
                    }

                    return $model->$attribute;
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';
                },
                'showModelErrors' => true,
                'errorOptions' => ['header' => ''],
                // 'postOnly' => true,
                'ajaxOnly' => true,
                // 'findModel' => function($id, $action) {},
                // 'checkAccess' => function($action, $model) {}
            ]
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $sale_id = $model->sale_id;
        $model->delete();

        if (\Yii::$app->request->isAjax) {
            $sale = Sales::findOne($sale_id);
            return $this->renderAjax('index', ['model' => $sale]);
        }
    }

    /**
     * Finds the Sales model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SaleItems the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = SaleItems::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}