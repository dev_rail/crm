<?php

namespace frontend\models;


use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class Roles extends Model
{
    public static function getAll()
    {
        $auth = Yii::$app->authManager;
        $roles = $auth->getRoles();
        unset($roles["audit"]);

        return ArrayHelper::map($roles, "name", "description");
    }

    public static function getRoleByName($name)
    {
        $auth = Yii::$app->authManager;

        $role = $auth->getRole($name);
        return $role;
    }

    public static function setRole($role_name, $user_id)
    {
        $auth = Yii::$app->authManager;

        $role = $auth->getRole($role_name);

        $auth->revokeAll($user_id);

        $auth->assign($role, $user_id);
    }

    public static function getRole($user_id)
    {
        $auth = Yii::$app->authManager;

        $role = $auth->getRolesByUser($user_id);

        $result = false;
        if ($role)
            $result = current($role);

        return $result;
    }
}