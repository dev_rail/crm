<?php

namespace frontend\models\objects;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use yii\helpers\ArrayHelper;

class Objects extends \common\models\objects\Objects
{
    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'position' => 'Координаты',
            'nameText' => 'Название'
        ], parent::attributeLabels());
    }


    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function getPosition()
    {
        return $this->lat." ".$this->lng;
    }

    public static function getAllArrayList($all = false)
    {
        $result = static::find()->processing()->all();
        $result = ArrayHelper::map($result, 'id', 'nameText');

        if($all)
            $result = ArrayHelper::merge(['Все'], $result);

        return $result;
    }

    public function getNameText()
    {
        $name = $this->name;

        $company = \Yii::$app->user->identity->company;
        if ($company->id != $this->company_id)
        {
            $prefix = $this->company->name;
            $name = "(".$prefix.") ".$name;
        }

        return $name;
    }
}