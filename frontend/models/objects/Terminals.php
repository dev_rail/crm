<?php

namespace frontend\models\objects;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use yii\helpers\ArrayHelper;

class Terminals extends \common\models\objects\Terminals
{
    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'object.name' => 'Объект'
        ], parent::attributeLabels());
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}