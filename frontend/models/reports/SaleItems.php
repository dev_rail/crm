<?php

namespace frontend\models\reports;


use frontend\components\CompanyBehavior;
use frontend\models\sales\Sales;
use yii\helpers\ArrayHelper;

class SaleItems extends \frontend\models\sales\SaleItems
{
    public $sumCount;
    public $sumMoney;

    public $type_pay;

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'product.nameText' => 'Продукт',
            'sumCount' => "Количество",
            'sumMoney' => "Сумма",
            'typePayText' => 'Тип оплаты',
            'sale.partner.name' => 'Контрагент',
            'sale.saleCard.card.name' => 'Карта',
            'sale.object.name' => 'Объект',
            'sale.object.nameText' => 'Объект'
        ]);
    }

    public function getTypePayText()
    {
        return Sales::TYPES_PAY[$this->type_pay];
    }
}