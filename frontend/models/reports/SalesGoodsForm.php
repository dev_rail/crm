<?php

namespace frontend\models\reports;

use frontend\components\CompanyQuery;
use frontend\models\reports\SaleItems;
use frontend\models\sales\Sales;
use kartik\grid\DataColumn;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class SalesGoodsForm extends Model
{
    public $date_start;
    public $date_end;

    public $object;
    public $type_pay;

    public $is_pay_type;

    public function rules()
    {
        return [
            [['date_start', 'date_end'], 'date', 'format' => 'php: d.m.Y H:i'],
            [['object', 'type_pay'], 'integer'],
            ['is_pay_type', 'boolean']
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_start' => 'Дата начала',
            'date_end' => 'Дата конца',
            'object' => 'Объект',
            'type_pay' => 'Тип оплаты',
            'is_pay_type' => 'Разбить по типу оплат'
        ];
    }

    public function search()
    {
        $time_start = $this->getTimeStart();
        $time_end = $this->getTimeEnd();

        $query = SaleItems::find()
            ->select([
                'sale_items.product_id',
                'SUM(sale_items.sum) as sumMoney',
                'SUM(count) as sumCount'
            ])
            ->joinWith('sale')
            ->where(['>=', 'date', $time_start])
            ->andWhere(['sales.company_id' => \Yii::$app->user->identity->company_id])
            ->andWhere(['<=', 'date', $time_end])
            ->groupBy('sale_items.product_id');

        if ($this->object)
            $query = $query->joinWith('sale.saleObjects')->andWhere(['sale_object.object_id' => $this->object]);

        if ($this->type_pay)
            $query = $query->andWhere(['type_pay' => $this->type_pay]);

        if ($this->is_pay_type)
            $query = $query->addGroupBy(['sales.type_pay'])->addSelect(['sales.type_pay']);

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    public function getVisibleColumns()
    {
        $result[] = [
            'attribute' => 'product.nameText',
            'pageSummary' => 'Итого:'
        ];

        if ($this->is_pay_type)
            $result[] = [
                'attribute' => 'typePayText'
            ];

        $result[] = [
            'class' => DataColumn::class,
            'attribute' => 'sumCount',
            'pageSummary' => true
        ];

        $result[] = [
            'class' => DataColumn::class,
            'attribute' => 'sumMoney',
            'pageSummary' => true
        ];;

        return $result;
    }

    private function getTimeStart()
    {
        $date = $this->date_start;

        if (!$date)
            $date = date("d.m.Y 00:00:00");

        return strtotime($date);
    }

    private function getTimeEnd()
    {
        $date = $this->date_end;

        if (!$date)
            $date = date("d.m.Y 23:59:59");

        return strtotime($date);
    }
}