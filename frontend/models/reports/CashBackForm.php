<?php

namespace frontend\models\reports;


use yii\base\Model;
use yii\data\ActiveDataProvider;

class CashBackForm extends Model
{
    public $date_start;
    public $date_end;

    public $object;
    public $partner;

    public function rules()
    {
        return [
            [['date_start', 'date_end'], 'date', 'format' => 'php: d.m.Y H:i'],
            [['object', 'partner'], 'integer']
        ];
    }

    public function attributeLabels()
    {
        return [
            'date_start' => 'Дата начала',
            'date_end' => 'Дата конца',
            'object' => 'Объект',
            'partner' => 'Контрагент'
        ];
    }

    private function getTimeStart()
    {
        $date = $this->date_start;

        if (!$date)
            $date = date("d.m.Y 00:00:00");

        return strtotime($date);
    }

    private function getTimeEnd()
    {
        $date = $this->date_end;

        if (!$date)
            $date = date("d.m.Y 23:59:59");

        return strtotime($date);
    }

    public function search()
    {
        $time_start = $this->getTimeStart();
        $time_end = $this->getTimeEnd();

        $query = SaleItems::find()
            ->joinWith('sale')
            ->joinWith('saleItemCashBack cash')
            ->where(['>=', 'date', $time_start])
            ->andWhere(['not', ['cash.id' => null]])
            ->andWhere(['sales.company_id' => \Yii::$app->user->identity->company_id])
            ->andWhere(['<=', 'date', $time_end]);

        if ($this->object)
            $query = $query->joinWith('sale.saleObjects')->andWhere(['sale_object.object_id' => $this->object]);

        if ($this->partner)
            $query = $query->andWhere(['partner_id' => $this->partner]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }
}