<?php

namespace frontend\models\partners;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use frontend\components\PartnersQuery;
use yii\helpers\ArrayHelper;

class Partners extends \common\models\partners\Partners
{
    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'countCards' => 'Количество карт'
        ], parent::attributeLabels());
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new PartnersQuery(get_called_class());
    }

    public function getCards()
    {
        return $this->hasMany(Cards::class, ['partner_id' => 'id']);
    }

    public function getCountCards()
    {
        return $this->getCards()->count();
    }

    public static function getAllArrayList($all = false)
    {
        $result = \Yii::$app->arrayList->get(static::class, "id", "name");

        if($all)
            $result = ArrayHelper::merge(['Все'], $result);

        return $result;
    }
}
