<?php

namespace frontend\models\partners;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use yii\helpers\ArrayHelper;

class Cards extends \common\models\partners\Cards
{
    public function init()
    {
        parent::init();
        $this->on(self::EVENT_BEFORE_VALIDATE, [$this, "convertUid"]);
    }

    public function convertUid()
    {
        $uid = $this->uid;

        if (is_numeric($uid))
        {
            $hex = dechex($uid);

            for($i = 0; $i < strlen($hex); $i += 2)
            {
                $result[] = mb_strtoupper($hex[$i].$hex[$i+1]);
            }

            $result = array_reverse($result);

            $this->uid = implode("-", $result);
        }
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'partner.name' => 'Контрагент'
        ], parent::attributeLabels());
    }
}
