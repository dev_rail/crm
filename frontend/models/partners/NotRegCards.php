<?php

namespace frontend\models\partners;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use yii\helpers\ArrayHelper;

class NotRegCards extends \common\models\partners\NotRegCards
{
    public $partner_id;
    public $number;
    public $name;

    public function rules()
    {
        return ArrayHelper::merge([
            [['partner_id', 'number', 'name'], 'required'],
            [['partner_id', 'number'], 'integer'],
            ['name', 'string']
        ], parent::rules());
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'partner_id' => 'Контрагент',
            'number' => 'Номер',
            'name' => 'Наименование'
        ], parent::attributeLabels());
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function registerCard()
    {
        $model = new Cards([
            'number' => $this->number,
            'partner_id' => $this->partner_id,
            'name' => $this->name,
            'uid' => $this->uid
        ]);

        if ($model->validate()) {
            $model->save();

            $this->delete();
            return true;
        }

        return false;
    }
}