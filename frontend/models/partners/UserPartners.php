<?php


namespace frontend\models\partners;


use common\models\partners\UsersPartners;
use yii\helpers\ArrayHelper;

class UserPartners extends UsersPartners
{
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['user_id', 'partner_id'], 'unique', 'targetAttribute' => ['user_id', 'partner_id'], 'message' => 'Контрагент уже добавлен к пользователю!']
        ]);
    }
}
