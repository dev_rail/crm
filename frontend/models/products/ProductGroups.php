<?php

namespace frontend\models\products;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class ProductGroups extends \common\models\products\ProductGroups
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'nameText' => 'Название'
        ]);
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function addCategory()
    {
        $model = new static([
            'parent_id' => $this->id
        ]);

        $model->save();
    }

    public function addProduct()
    {
        $this->link('products', new Products());
    }

    public function getChildrenCategoryDataProvider()
    {
        $query  = $this->getProductGroups();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }

    public function getProductsDataProvider()
    {
        $query = $this->getProducts();

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }

    public function getBreadcrumbs()
    {
        $groups = [];

        $model = $this->parent;

        while ($model)
        {
            $groups[] = ['label' => $model->nameText, 'url' => ['/product-groups/view', 'id' => $model->id]];
            $model = $model->parent;
        }

        $groups[] = ['label' => 'Группы продуктов', 'url' => ['index']];

        return array_reverse($groups);
    }
}