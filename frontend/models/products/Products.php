<?php

namespace frontend\models\products;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use yii\helpers\ArrayHelper;

class Products extends \common\models\products\Products
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public static function getAllArrayList()
    {
        return ArrayHelper::map(static::find()->processing()->all(), "id", "nameText");
    }

    public function getGroup()
    {
        return $this->hasOne(ProductGroups::class, ['id' => 'group_id']);
    }
}