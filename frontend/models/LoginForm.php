<?php

namespace frontend\models;


use api\modules\device\models\Sessions;
use frontend\models\Users;

class LoginForm extends \common\models\LoginForm
{
    private $_user;


    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Users::findByUsername($this->username, $this->password);
        }

        return $this->_user;
    }
}