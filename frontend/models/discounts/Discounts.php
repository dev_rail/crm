<?php

namespace frontend\models\discounts;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use frontend\models\partners\Cards;
use frontend\models\products\Products;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class Discounts extends \common\models\discounts\Discounts
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function attributeLabels()
    {
        return  ArrayHelper::merge(parent::attributeLabels(), [
            'product.name' => 'Продукт',
            'typeText' => 'Тип'
        ]);
    }

    public function getTypeText()
    {
        return self::TYPES[$this->type];
    }

    public function getDiscountCardsDataProvider()
    {
        $query = $this->getDiscountCards();

        return new ActiveDataProvider(['query' => $query, 'pagination' => false]);
    }

    public function getDiscountObjectsDataProvider()
    {
        $query = $this->getDiscountObjects();

        return new ActiveDataProvider(['query' => $query, 'pagination' => false]);
    }

    public function getAllCards()
    {
        $cards = $this->partner->cards;
        return ArrayHelper::map($cards, "id", "name");
    }

    public function addDiscountCard()
    {
        $model = new DiscountCards([
            'discount_id' => $this->id
        ]);

        $model->save();
    }

    public function addDiscountObject()
    {
        $model = new DiscountObjects([
            'discount_id' => $this->id
        ]);

        $model->save();
    }

    public function getDiscountCards()
    {
        return $this->hasMany(DiscountCards::class, ['discount_id' => 'id']);
    }

    public function getDiscountObjects()
    {
        return $this->hasMany(DiscountObjects::class, ['discount_id' => 'id']);
    }
}