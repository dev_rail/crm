<?php

namespace frontend\models\discounts;


use yii\helpers\ArrayHelper;

class DiscountCards extends \common\models\discounts\DiscountCards
{
    public function getTypeText()
    {
        return ArrayHelper::getValue(Discounts::TYPES, $this->type);
    }

    public function getDiscount()
    {
        return $this->hasOne(Discounts::class, ['id' => 'discount_id']);
    }
}