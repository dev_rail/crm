<?php

namespace frontend\models\discounts;


use yii\helpers\ArrayHelper;

class DiscountObjects extends \common\models\discounts\DiscountObjects
{
    public function getTypeText()
    {
        return ArrayHelper::getValue(Discounts::TYPES, $this->type);
    }

    public function getDiscount()
    {
        return $this->hasOne(Discounts::class, ['id' => 'discount_id']);
    }
}