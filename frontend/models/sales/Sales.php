<?php

namespace frontend\models\sales;

use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use frontend\components\SalesQuery;
use frontend\models\objects\Objects;
use yii\base\Event;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class Sales extends \common\models\sales\Sales
{
    private $_objectId;

    public function rules()
    {
        return ArrayHelper::merge([
            ['dateText', 'date', 'format' => 'php: d.m.Y H:i:s'],
            ['objectId', 'integer']
        ], parent::rules());
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'partner.name' => 'Контрагент',
            'dateText' => 'Дата',
            'objectId' => 'Объект',
            'object.name' => 'Объект',
            'object.nameText' => 'Объект'
        ], parent::attributeLabels());
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public function getObject()
    {
        return $this->hasOne(Objects::class, ['id' => 'objectId'])->processing();
    }

    public function init()
    {
        parent::init();
        $this->on(static::EVENT_AFTER_UPDATE, [$this, "updateObjectId"]);
        $this->on(static::EVENT_AFTER_INSERT, [$this, "updateObjectId"]);
    }

    public function updateObjectId()
    {
        if (!$this->saleObject)
        {
            $saleObject = new SaleObject([
                'user_id' => \Yii::$app->user->id
            ]);
            $this->link('saleObject', $saleObject);
        }
        else
            $saleObject = $this->saleObject;

        $saleObject->object_id = $this->_objectId;
        $saleObject->save();

    }

    public function getSaleObject()
    {
        return $this->hasOne(SaleObject::class, ['sale_id' => 'id']);
    }

    public function getSaleItems()
    {
        return $this->hasMany(SaleItems::class, ['sale_id' => 'id']);
    }

    public static function find()
    {
        return new SalesQuery(get_called_class());
    }

    public function getDateText()
    {
        if (!$this->date)
            return date("d.m.Y H:i:s");

        return date("d.m.Y H:i:s", $this->date);
    }

    public function setDateText($value)
    {
        $this->date = strtotime($value);
    }

    public function getObjectId()
    {
        return ArrayHelper::getValue($this, "saleObject.object_id");
    }

    public function setObjectId($object_id)
    {
        $this->_objectId = $object_id;
    }

    public function getItemsDataProvider()
    {
        return new ActiveDataProvider([
            'pagination' => false,
            'query' => $this->getSaleItems()
        ]);
    }

    public function addItem()
    {
        $model = new SaleItems();
        $this->link('saleItems', $model);
    }
}
