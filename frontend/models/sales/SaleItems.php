<?php

namespace frontend\models\sales;


class SaleItems extends \common\models\sales\SaleItems
{
    const EVENT_CHANGE_PRODUCT = 'change-product';

    public function init()
    {
        parent::init();

        $this->on(static::EVENT_CHANGE_PRODUCT, [$this, "changeProduct"]);
    }

    public function changeProduct()
    {
        $product = $this->product;
        $this->price = $product->price;
        $this->count = 1;
    }

    public function beforeSave($insert)
    {
        if ($this->getOldAttribute('product_id') != $this->product_id)
            $this->trigger(static::EVENT_CHANGE_PRODUCT);

        return parent::beforeSave($insert);
    }

    public function getSale()
    {
        return $this->hasOne(Sales::class, ['id' => 'sale_id']);
    }
}