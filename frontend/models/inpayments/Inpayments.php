<?php

namespace frontend\models\inpayments;


use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use frontend\components\InpaymentsQuery;
use yii\helpers\ArrayHelper;

class Inpayments extends \common\models\inpayments\Inpayments
{
    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'dateText' => 'Дата',
            'partner.name' => 'Контрагент'
        ], parent::attributeLabels());
    }

    public function rules()
    {
        return ArrayHelper::merge([
            ['dateText', 'date', 'format' => 'php: d.m.Y H:i:s']
        ], parent::rules());
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new InpaymentsQuery(get_called_class());
    }

    public function getDateText()
    {
        if (!$this->date)
            return date("d.m.Y H:i:s");

        return date("d.m.Y H:i:s", $this->date);
    }

    public function setDateText($value)
    {
        $this->date = strtotime($value);
    }
}
