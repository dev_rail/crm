<?php

namespace frontend\models;


use common\models\partners\PartnerProcessing;
use yii\helpers\ArrayHelper;

class Company extends \common\models\Company
{
    public function getProcessingIds()
    {
        $processings = PartnerProcessing::find()
            ->where(['company_id' => $this->id])
            ->all();

        return ArrayHelper::getColumn($processings, "seller_company_id");
    }
}