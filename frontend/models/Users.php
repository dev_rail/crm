<?php

namespace frontend\models;


use common\models\CompanyUsers;
use frontend\components\CompanyBehavior;
use frontend\components\CompanyQuery;
use frontend\models\partners\UserPartners;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\IdentityInterface;

class Users extends CompanyUsers implements IdentityInterface
{
    private $_role;

    public function updateRole()
    {
        if (!$this->_role)
            return false;

        Roles::setRole($this->_role, $this->id);
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_UPDATE, [$this, "updateRole"]);
        $this->on(self::EVENT_AFTER_INSERT, [$this, "updateRole"]);
    }

    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['role', 'required'],
            [['passwordOrigin', 'role'], 'string']
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge([
            'passwordOrigin' => 'Пароль',
            'role' => 'Роль',
            'roleName' => "Роль"
        ], parent::attributeLabels());
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // TODO: Implement findIdentityByAccessToken() method.
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    public function setPassword($password)
    {
        $this->password = \Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @param bool $password
     * @return static|null
     */
    public static function findByUsername($username, $password = false)
    {
        $result = static::find()->where(['username' => $username]);

        if ($password) {
            $result = $result->all();
            foreach ($result as $item)
                if ($item->validatePassword($password))
                    return $item;
        }
        else
        {
            $result = $result->one();
            return $result;
        }

        return null;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function setPasswordOrigin($value)
    {
        if ($value)
            $this->setPassword($value);
    }

    public function getPasswordOrigin()
    {
        return "";
    }

    public function getRoleName()
    {
        if ($this->_role)
            $role = Roles::getRoleByName($this->_role);
        else
            $role = Roles::getRole($this->id);

        return ArrayHelper::getValue($role, "description");
    }

    public function getRole()
    {
        if ($this->_role)
            $role = Roles::getRoleByName($this->_role);
        else
            $role = Roles::getRole($this->id);

        return ArrayHelper::getValue($role, "name");
    }

    public function setRole($value)
    {
        if (isset(Yii::$app->user) && $this->id == Yii::$app->user->id)
            throw new ForbiddenHttpException('Запрещено менять пользователю собственную роль!');


        $this->_role = $value;
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getUserPartners()
    {
        return $this->hasMany(UserPartners::class, ['user_id' => 'id']);
    }

    public function getPartnersDataProvider()
    {
        return new ActiveDataProvider([
            'query' => $this->getUserPartners(),
            'pagination' => false
        ]);
    }

    public function addPartner()
    {
        $model = new UserPartners();
        $this->link('userPartners', $model);
    }
}
