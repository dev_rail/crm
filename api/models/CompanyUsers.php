<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 05.11.18
 * Time: 11:08
 */

namespace api\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;

class CompanyUsers extends \common\models\CompanyUsers
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}