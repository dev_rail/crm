<?php

namespace api\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;

class Terminals extends \common\models\objects\Terminals
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}