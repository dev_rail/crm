<?php

namespace api\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;

class NotRegCards extends \common\models\partners\NotRegCards
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}