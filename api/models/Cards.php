<?php

namespace api\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;

class Cards extends \common\models\partners\Cards
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}