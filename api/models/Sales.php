<?php

namespace api\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;

class Sales extends \common\models\sales\Sales
{
    public function behaviors()
    {
        return [
            'companyBehavior' => [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}