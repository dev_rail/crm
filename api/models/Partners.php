<?php

namespace api\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;

class Partners extends \common\models\partners\Partners
{
    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }
}