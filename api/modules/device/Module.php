<?php

namespace api\modules\device;

use api\modules\device\models\Users;

/**
 * device module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'api\modules\device\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        \Yii::$app->user->enableSession = false;
        \Yii::$app->user->identityClass = Users::class;
    }
}
