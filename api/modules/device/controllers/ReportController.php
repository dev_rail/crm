<?php

namespace api\modules\device\controllers;


use api\controllers\BaseController;
use api\controllers\BaseWebController;
use api\modules\device\models\report\SaleReport;
use yii\web\Controller;
use yii\web\Response;

class ReportController extends BaseWebController
{
    public $layout = 'main';

    public function actionSaleProducts($start = false, $end = false)
    {
        $model = new SaleReport([
            'start' => $start,
            'end' => $end,
            'terminal' => \Yii::$app->user->identity->session->terminal_id,
            'user_id' => \Yii::$app->user->identity->session->user_id
        ]);

        return $this->render('sale-products', ['model' => $model]);
    }
}