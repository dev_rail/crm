<?php

namespace api\modules\device\controllers;

use api\controllers\BaseController;
use api\modules\device\models\report\SaleReport;
use api\modules\device\models\SaleForm;
use api\modules\device\models\Sales;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

class SaleController extends BaseController
{
    public $modelClass = Sales::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'new' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        unset($actions['index'], $actions['create'], $actions['view'], $actions['delete'], $actions['update']);

        return $actions;
    }

    public function actionNew()
    {
        $post = \Yii::$app->request->post();

        $model = new SaleForm([
            'card_id' => ArrayHelper::getValue($post, 'card_id'),
            'items' => ArrayHelper::getValue($post, 'items'),
            'session_id' => \Yii::$app->user->identity->session->id,
            'type_pay' => ArrayHelper::getValue($post, 'type_pay')
        ]);

        if ($model->validate())
            return $model->save();
        else
            throw new HttpException(400, implode(",", $model->firstErrors));
    }

    public function actionReport($start = false, $end = false)
    {
        if (!$start or $start == " ")
            $start = date("d.m.Y H:i", \Yii::$app->user->identity->session->created_at);

        $model = new SaleReport([
            'start' => $start,
            'end' => $end,
            'terminal' => \Yii::$app->user->identity->session->terminal_id,
            'user_id' => \Yii::$app->user->identity->session->user_id
        ]);

        return $model->search();
    }

    public function actionReportProduct($start = false, $end = false)
    {
        if (!$start or $start == " ")
            $start = date("d.m.Y H:i", \Yii::$app->user->identity->session->created_at);

        $model = new SaleReport([
            'start' => $start,
            'end' => $end,
            'terminal' => \Yii::$app->user->identity->session->terminal_id,
            'user_id' => \Yii::$app->user->identity->session->user_id
        ]);

        return $model->searchGroupProduct();
    }
}