<?php

namespace api\modules\device\controllers;


use api\controllers\BaseController;
use api\modules\device\models\Cards;
use api\modules\device\models\NotRegCards;
use api\modules\device\models\Products;
use yii\web\NotFoundHttpException;

class CardController extends BaseController
{
   public $modelClass = Cards::class;

   public function actions()
   {
       $actions = parent::actions();

       unset($actions['index'], $actions['update'], $actions['delete'], $actions['view'], $actions['delete']);

       return $actions;
   }

   public function actionView($uid)
   {
       $model = Cards::find()
           ->processing()
           ->andWhere(['uid' => $uid])
           ->one();

       if (!$model) {
            NotRegCards::addCard($uid, \Yii::$app->user->identity->session->id);
            throw new NotFoundHttpException('no card');
       }

       Products::$object_id = \Yii::$app->user->identity->session->object_id;
       Products::$card_id = $model->id;

       return $model;
   }
}