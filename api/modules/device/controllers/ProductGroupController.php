<?php

namespace api\modules\device\controllers;


use api\controllers\BaseController;
use api\modules\device\models\ProductGroups;
use yii\data\ActiveDataProvider;

class ProductGroupController extends BaseController
{
    public $modelClass = ProductGroups::class;

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $query = ProductGroups::find()->andWhere(['parent_id' => null]);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => false
        ]);
    }
}