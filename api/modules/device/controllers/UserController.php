<?php

namespace api\modules\device\controllers;


use api\controllers\BaseController;
use api\modules\device\models\LoginForm;
use api\modules\device\models\Users;
use yii\filters\VerbFilter;

class UserController extends BaseController
{
    public $modelClass = Users::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator']['except'] = ['login', 'options'];

        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'login' => ['POST'],
            ],
        ];

        return $behaviors;
    }

    public function actionLogin($imei, $username, $password)
    {
        $model = new LoginForm([
           'imei' => $imei,
           'username' => $username,
           'password' => $password
        ]);

        return $model->login();
    }

    public function actionLogout()
    {
        $session = \Yii::$app->user->identity->session;
        return $session->close();
    }
}