<?php

namespace api\modules\device\models;


use yii\helpers\ArrayHelper;

class SaleItems extends \api\models\SaleItems
{
    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'product'
        ]);
    }
}