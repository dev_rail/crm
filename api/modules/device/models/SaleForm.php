<?php

namespace api\modules\device\models;

use api\modules\device\models\TerminalSessions;
use common\models\sales\SaleItemCashBack;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class SaleForm extends Model
{
    public $card_id;
    public $items;
    public $session_id;
    public $type_pay;

    private $_card;
    private $_session;

    public function rules()
    {
        return [
            [['session_id', 'items', 'card_id'], 'required'],
            [['card_id', 'session_id', 'type_pay'], 'integer'],
            ['type_pay', 'default', 'value' => Sales::TYPE_PAY_ELECTRON],
            ['card_id', 'validateCard'],
            ['items', 'validateItems']
        ];
    }

    public function save()
    {
        /** @var Cards $card */
        $card = $this->card;

        if ($card->isCurrentProcessing()) {
            $sale = new Sales([
                'date' => time(),
                'partner_id' => $this->card->partner_id,
                'type_pay' => $this->type_pay
            ]);
            $sale->save();

            $sale->link('saleCard', new SaleCard(['card_id' => $this->card_id]));

            $sale->link('saleObject', new SaleObject([
                'session_id' => $this->session_id,
                'terminal_id' => $this->session->terminal_id,
                'object_id' => $this->session->object_id,
                'user_id' => $this->session->user_id
            ]));

            $object_id = $this->session->object_id;
            $card_id = $this->card_id;

            Products::$card_id = $card_id;
            Products::$object_id = $object_id;

            foreach ($this->items as $item) {
                $product = Products::findOne($item['product_id']);

                $price = $product->activePrice;

                $sale->link('saleItems', new SaleItems([
                    'product_id' => $item['product_id'],
                    'count' => $item["count"],
                    'price' => $price,
                    'actual_price' => $price
                ]));
            }

            return Sales::findOne($sale->id);
        }
        else
        {
            /** @var Company $company */
            $company = $this->session->company;

            $current_partner = $company->getPartnerByProcessing($card->company_id);

            $current_sale = new Sales([
                'date' => time(),
                'partner_id' => $current_partner->id,
                'type_pay' => $this->type_pay
            ]);
            $current_sale->save();

            $current_sale->link('saleCard', new SaleCard(['card_id' => $this->card_id]));

            $current_sale->link('saleObject', new SaleObject([
                'session_id' => $this->session_id,
                'terminal_id' => $this->session->terminal_id,
                'object_id' => $this->session->object_id,
                'user_id' => $this->session->user_id
            ]));

            $object_id = $this->session->object_id;
            $card_id = null;

            Products::$card_id = $card_id;
            Products::$object_id = $object_id;
            Products::$partner_id = $current_partner->id;

            foreach ($this->items as $item) {
                $product = Products::findOne($item['product_id']);

                $price = $product->activePrice;

                $current_sale->link('saleItems', new SaleItems([
                    'product_id' => $item['product_id'],
                    'count' => $item["count"],
                    'price' => $price
                ]));
            }

            $sale = new Sales([
                'company_id' => $card->company_id,
                'date' => time(),
                'partner_id' => $card->partner->id,
                'type_pay' => $this->type_pay
            ]);

            $sale->save();

            $sale->link('saleObject', new SaleObject([
                'session_id' => $this->session_id,
                'terminal_id' => $this->session->terminal_id,
                'object_id' => $this->session->object_id,
                'user_id' => $this->session->user_id
            ]));

            $sale->link('saleCard', new SaleCard(['card_id' => $this->card_id]));

            Products::$card_id = $card->id;
            Products::$object_id = $object_id;
            Products::$partner_id = null;

            foreach ($this->items as $item) {
                $product = Products::findOne($item['product_id']);

                $price = $product->activePrice;

                $current_sale_item = $current_sale->getSaleItems()->where(['product_id' => $product->id])->one();
                $current_sale_item->actual_price = $price;
                $current_sale_item->save();

                if ($current_sale->type_pay == Sales::TYPE_PAY_CASH)
                    $current_sale_item->link("saleItemCashBack", new SaleItemCashBack(['price' => $price]));

                $sale->link('saleItems', new SaleItems([
                    'product_id' => $item['product_id'],
                    'count' => $item["count"],
                    'price' => $price,
                    'actual_price' => $price
                ]));
            }

            return $current_sale;
        }
    }

    public function getCard()
    {
        if ($this->_card)
            return $this->_card;

        return Cards::find()->processing()->where(['id' => $this->card_id])->one();
    }

    public function getSession()
    {
        if ($this->_session)
            return $this->_session;

        return TerminalSessions::findOne($this->session_id);
    }

    public function validateCard()
    {
        if (!$this->card)
            $this->addError('card_id', 'no card');
    }


    public function validateItems()
    {
        $sum = 0;
        $sum_count = 0;
        foreach ($this->items as $item)
        {
            $product_id = ArrayHelper::getValue($item, "product_id");
            $count = ArrayHelper::getValue($item, "count");

            $this->validProduct($product_id);

            if (!is_int($count) && !is_double($count))
                $this->addError('items', 'count must be digital');

            $price = $this->card->getPriceByProductId($product_id);
            $sum += $price * $count;
            $sum_count += $count;
        }

        if ($sum >= 10000000)
            $this->addError('items', 'Сумма должна быть меньше 10 000 000');

        switch ($this->type_pay)
        {
            case Sales::TYPE_PAY_ELECTRON:
                $card = $this->card;
                if ($card->enabledBalanceSum - $sum < 0)
                    $this->addError('items', 'Недостаточно средств на балансе! Текущий баланс: '.$card->enabledBalanceSum);
                break;
            case Sales::TYPE_PAY_BONUS:
                /** @var Cards $card */
                $card = $this->card;
                if ($card->isCurrentProcessing()) {
                    if ($sum > $card->bonus_balance)
                        $this->addError('type_pay', 'Недостаточно средств на бонусном счете!');
                }
                else
                    $this->addError('type_pay', 'Списание с бонусного счета запрещено!');
                break;
            default:
                break;
        }

        $card = $this->card;

        if ($card->period_type)
        {
            switch ($card->period_type)
            {
                case Cards::PERIOD_TYPE_DAY:
                    $date_start = date("d.m.Y 00:00:00");
                    $date_end = date("d.m.Y 23:59:59");
                    break;
                case Cards::PERIOD_TYPE_WEEK:
                    $date_start = date("d.m.Y 00:00:00", strtotime('monday this week'));
                    $date_end = date("d.m.Y 23:59:59", strtotime('sunday this week'));
                    break;
                case Cards::PERIOD_TYPE_MONTH:
                    $date_start = date("01.m.Y 00:00:00");
                    $date_end = date("t.m.Y 23:59:59");
                    break;
            }

            $date_start_time = strtotime($date_start);
            $date_end_time = strtotime($date_end);

            switch ($card->limit_type)
            {
                case Cards::LIMIT_TYPE_COUNT:
                    $r_sum  = $sum_count + $card->getSaleItemsCountByPeriod($date_start_time, $date_end_time);
                    break;
                case Cards::LIMIT_TYPE_MONEY:
                    $r_sum = $sum + $card->getSaleItemsSumByPeriod($date_start_time, $date_end_time);
                    break;
            }

            if ($card->limit_value <= $r_sum)
                $this->addError('items', 'Лимит карты исчерпан!');
        }
    }

    private function validProduct($product_id)
    {
        if (!$product_id) {
            $this->addError('items', 'product_id is null');
            return;
        }

        if (!is_int($product_id)) {
            $this->addError('items', 'product_id must be integer');
            return;
        }

        $product = Products::findOne($product_id);

        if (!$product)
        {
            $this->addError('items', 'no product id:'.$product_id);
            return;
        }
    }
}