<?php

namespace api\modules\device\models;


use common\models\objects\Terminals;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class LoginForm extends \common\models\LoginForm
{
    private $_user;

    public $imei;

    public function rules()
    {
        return ArrayHelper::merge([
            ['imei', 'validateImei']
        ], parent::rules());
    }

    public function validateImei()
    {
        $terminal = $this->terminal;

        if (!$terminal)
        {
            $this->addError('imei', 'Терминал не добавлен в базу!');
        }
    }

    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Users::findByUsername($this->username);
        }

        return $this->_user;
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool
     */
    public function login()
    {
        if ($this->validate()) {
            return $this->_user->addSession($this->terminal->id);
        }

        return $this->firstErrors;
    }

    public function getTerminal()
    {
        return Terminals::findOne(['imei' => $this->imei]);
    }
}