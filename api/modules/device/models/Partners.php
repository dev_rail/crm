<?php

namespace api\modules\device\models;


class Partners extends \api\models\Partners
{
    public function fields()
    {
        return ['id', 'name'];
    }
}