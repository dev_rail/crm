<?php

namespace api\modules\device\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;
use api\models\CompanyUsers;
use api\models\Terminals;
use phpDocumentor\Reflection\Types\Static_;
use yii\web\IdentityInterface;

class Users extends CompanyUsers implements IdentityInterface
{
    public static $token;
    /**
     * Finds an identity by the given ID.
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }

    /**
     * Finds an identity by the given token.
     * @param mixed $token the token to be looked for
     * @param mixed $type the type of the token. The value of this parameter depends on the implementation.
     * For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be `yii\filters\auth\HttpBearerAuth`.
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = Users::find()->joinWith('sessionLink s')
            ->where(['token' => $token])
            ->andWhere(['close' => false])
            ->one();

        Users::$token = $token;

        return $user;
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     * @return string|int an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        // TODO: Implement getAuthKey() method.
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     * @param string $authKey the given auth key
     * @return bool whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        // TODO: Implement validateAuthKey() method.
    }

    public function getSessionLink()
    {
        return $this->hasOne(Sessions::class, ['user_id' => 'id'])->where(['close' => false]);
    }

    public function getSession()
    {
        return $this->hasOne(Sessions::class, ['user_id' => 'id'])->where(['close' => false])->andFilterWhere(['token' => Users::$token]);
    }


    public function addSession($terminal_id)
    {
        $terminal = Terminals::findOne($terminal_id);
        $session = new Sessions([
            'company_id' => $this->company_id,
            'token' => md5(uniqid($this->company_id, "sha256")),
            'user_id' => $this->id,
            'created_at' => time(),
            'terminal_id' => $terminal_id,
            'object_id' => $terminal->object_id
        ]);

        $session->save();

        return $session;
    }

    public function validatePassword($password)
    {
        return \Yii::$app->security->validatePassword($password, $this->password);
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }
}