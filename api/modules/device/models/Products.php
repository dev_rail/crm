<?php

namespace api\modules\device\models;


class Products extends \common\models\products\Products
{
    public static $card_id;
    public static $object_id;
    public static $partner_id;

    public function fields()
    {
        return ['id', 'name', 'price', 'activePrice'];
    }

    public function getActivePrice()
    {
        if (static::$partner_id && static::$object_id && !static::$card_id)
        {
                return $this->getPriceByPartnerAndObject(static::$partner_id, static::$object_id);
        }

        if (!static::$card_id or !static::$object_id)
            return $this->price;

        return $this->getPriceByCardAndObject(static::$card_id, static::$object_id);
    }
}