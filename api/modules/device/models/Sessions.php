<?php

namespace api\modules\device\models;


use api\models\TerminalSessions;

class Sessions extends TerminalSessions
{
    public static $token_active;

    public function init()
    {
        parent::init();
        $this->on(static::EVENT_BEFORE_INSERT, [$this, "closeSessions"]);
    }

    public function closeSessions()
    {
        $sessions = static::find()->where(['terminal_id' => $this->terminal_id, 'close' => false])->all();

        foreach ($sessions as $session)
            $session->close();
    }

    public function close()
    {
        $this->closed_at = time();
        $this->close = true;
        $this->save();
    }
}