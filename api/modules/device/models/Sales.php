<?php

namespace api\modules\device\models;


use api\components\CompanyQuery;

class Sales extends \api\models\Sales
{
    public function fields()
    {
        return ['id', 'dateText', 'sum'];
    }

    public function extraFields()
    {
        return ['saleItems'];
    }

    public function getSaleItems()
    {
        return $this->hasMany(SaleItems::class, ['sale_id' => 'id']);
    }

    public function getSaleCard()
    {
        return $this->hasOne(SaleCard::class, ['sale_id' => 'id']);
    }

    public function getSaleObject()
    {
        return $this->hasOne(SaleObject::class, ['sale_id' => 'id']);
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}