<?php

namespace api\modules\device\models;


use common\models\partners\PartnerProcessing;
use yii\helpers\ArrayHelper;

class Company extends \api\models\Company
{
    public function getProcessingIds()
    {
        $processing = PartnerProcessing::find()->where(['seller_company_id' => $this->id])->all();
        return ArrayHelper::getColumn($processing, "company_id");
    }

    public function getPartnerByProcessing($company_id)
    {
        $partner = $this->getPartners()->joinWith('processing p')
            ->andWhere(['p.company_id' => $company_id])
            ->one();

        return $partner;
    }
}