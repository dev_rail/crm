<?php

namespace api\modules\device\models\report;


use api\modules\device\models\Sales;
use yii\helpers\ArrayHelper;

class SaleItems extends \api\modules\device\models\SaleItems
{
    public $sumCount;
    public $sumMoney;

    public $type_pay;

    public function fields()
    {
        $fields = parent::fields();

        unset($fields[array_search('product', $fields)]);

        return ArrayHelper::merge($fields, [
            'sumCount',
            'sumMoneyText',
            'type_pay',
            'typePayText',
            'productName',
            'actual_price'
        ]);
    }

    public function getSumMoneyText()
    {
        return $this->sumMoney;
    }

    public function getTypePayText()
    {
        return Sales::TYPES_PAY[$this->type_pay];
    }

    public function getProductName()
    {
        return ArrayHelper::getValue($this->product, "name");
    }
}