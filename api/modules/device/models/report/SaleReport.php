<?php

namespace api\modules\device\models\report;


use api\models\Terminals;
use api\modules\device\models\report\SaleItems;
use api\modules\device\models\Sales;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class SaleReport extends Model
{
    public $start;
    public $end;

    public $terminal;
    public $user_id;

    public function rules()
    {
        return [
            [['terminal', 'user_id'], 'required']
        ];
    }

    public function search()
    {
        if (!$this->validate())
            return $this->firstErrors;

        $query = Sales::find()->where(['>=', 'date', $this->getDateStart()])
            ->andWhere(['<=', 'date', $this->getDateEnd()])
            ->joinWith('saleObject')
            ->andWhere(['terminal_id' => $this->terminal]);

        $auth = Yii::$app->authManager;
        $role = $auth->getRolesByUser($this->user_id);
        $role = current($role);

        if ($role->name != "admin")
            $query = $query->andWhere(['user_id' => $this->user_id]);

        return new ActiveDataProvider(['query' => $query, 'pagination' => false]);
    }

    public function getDateStart()
    {
        $date = $this->start;

        if (!$date or $date == " ")
            $date = date("d.m.Y 00:00:00");

        return strtotime($date);
    }

    public function getDateEnd()
    {
        $date = $this->end;

        if (!$date or $date == " ")
            $date = date("d.m.Y 23:59:59");

        return strtotime($date);
    }

    public function searchGroupProduct()
    {
        if (!$this->validate())
            return $this->firstErrors;

        $terminal = Terminals::findOne($this->terminal);

        $query = SaleItems::find()->where(['>=', 'date', $this->getDateStart()])
            ->select([
                'sale_items.product_id',
                'SUM(sale_items.actual_sum) as sumMoney',
                'SUM(count) as sumCount',
                'actual_price'
            ])
            ->andWhere(['<=', 'date', $this->getDateEnd()])
            ->joinWith('sale')
            ->joinWith('sale.saleObjects')
            ->andWhere(['terminal_id' => $this->terminal])
            ->andWhere(['sales.company_id' => $terminal->company_id])
            ->groupBy(['actual_price', 'product_id'])
            ->addGroupBy(['sales.type_pay'])->addSelect(['sales.type_pay']);

        $auth = Yii::$app->authManager;
        $role = $auth->getRolesByUser($this->user_id);
        $role = current($role);

        if ($role->name != "admin")
            $query = $query->andWhere(['user_id' => $this->user_id]);

        return new ActiveDataProvider(['query' => $query, 'pagination' => false]);
    }
}