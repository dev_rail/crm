<?php

namespace api\modules\device\models;

class Cards extends \api\models\Cards
{
    public function fields()
    {
        return ['id', 'name', 'number', 'bonus_balance', 'partner', 'uid', 'productGroups', 'typesPay', 'availableLimitText'];
    }

    public function getPartner()
    {
        return $this->hasOne(Partners::class, ['id' => 'partner_id']);
    }

    public function getProductGroups()
    {
        $groups = ProductGroups::find()->where(['not', ['name' => null]])->all();

        return $groups;
    }

    public function getEnabledBalanceSum()
    {
        $company_id = \Yii::$app->user->identity->company_id;

        if ($this->company_id == $company_id)
        {
            return $this->partner->enabledBalanceSum;
        }
        else
        {
            /** @var Company $company */
            $company = \Yii::$app->user->identity->company;
            $partner = $company->getPartnerByProcessing($this->company_id);

            $processing_sum = $partner->enabledBalanceSum;
            $sum = $this->partner->enabledBalanceSum;

            if ($sum >= $processing_sum)
                return $processing_sum;
            else
                return $sum;
        }
    }

    public function isCurrentProcessing()
    {
        $company_id = \Yii::$app->user->identity->company_id;

        if ($this->company_id == $company_id)
            return true;
        else
            return false;
    }

    public function getPriceByProductId($product_id)
    {
        $product = Products::findOne($product_id);

        return $product->price;
    }

    public function getTypesPay()
    {
        $types = Sales::TYPES_PAY;
        $result = [];

        foreach ($types as $key => $value) {
            if (!$this->isCurrentProcessing() && $key == Sales::TYPE_PAY_BONUS)
                continue;

            $result[] = [
                'id' => $key,
                'name' => $value
            ];
        }

        return $result;
    }

    public function getAvailableLimit()
    {
        if ($this->period_type) {
            switch ($this->period_type) {
                case Cards::PERIOD_TYPE_DAY:
                    $date_start = date("d.m.Y 00:00:00");
                    $date_end = date("d.m.Y 23:59:59");
                    break;
                case Cards::PERIOD_TYPE_WEEK:
                    $date_start = date("d.m.Y 00:00:00", strtotime('monday this week'));
                    $date_end = date("d.m.Y 23:59:59", strtotime('sunday this week'));
                    break;
                case Cards::PERIOD_TYPE_MONTH:
                    $date_start = date("01.m.Y 00:00:00");
                    $date_end = date("t.m.Y 23:59:59");
                    break;
            }

            $date_start_time = strtotime($date_start);
            $date_end_time = strtotime($date_end);

            switch ($this->limit_type) {
                case Cards::LIMIT_TYPE_COUNT:
                    $r_sum = $this->getSaleItemsCountByPeriod($date_start_time, $date_end_time);
                    break;
                case Cards::LIMIT_TYPE_MONEY:
                    $r_sum = $this->getSaleItemsSumByPeriod($date_start_time, $date_end_time);
                    break;
            }

            return $this->limit_value - $r_sum;
        }
    }

    public function getAvailableLimitText()
    {
        switch ($this->limit_type)
        {
            case self::LIMIT_TYPE_COUNT:
                return $this->availableLimit;
                break;
            case  self::LIMIT_TYPE_MONEY:
                return $this->availableLimit." руб.";
                break;
            default:
                return "-";
                break;
        }
    }
}