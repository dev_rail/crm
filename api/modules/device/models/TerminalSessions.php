<?php

namespace api\modules\device\models;


class TerminalSessions extends \api\models\TerminalSessions
{
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }
}