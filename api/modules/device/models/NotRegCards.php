<?php

namespace api\modules\device\models;


class NotRegCards extends \api\models\NotRegCards
{
    public static function addCard(string $uid, int $session_id)
    {
        $session = Sessions::findOne($session_id);

        $model = new self([
            'uid' => $uid,
            'date' => time(),
            'terminal_id' => $session->terminal_id,
            'object_id' => $session->object_id
        ]);

        $model->save();
    }
}