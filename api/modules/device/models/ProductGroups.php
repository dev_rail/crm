<?php

namespace api\modules\device\models;


use api\components\CompanyBehavior;
use api\components\CompanyQuery;
use yii\helpers\ArrayHelper;

class ProductGroups extends \common\models\products\ProductGroups
{
    public function fields()
    {
        return ['id', 'parent_id', 'name', 'products', 'productGroups'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroups()
    {
        return $this->hasMany(ProductGroups::className(), ['parent_id' => 'id'])->where(['not', ['name' => null]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['group_id' => 'id'])->where(['not', ['name' => null]])->andWhere(['not', ['price' => null]]);
    }

    public function behaviors()
    {
        return [
            [
                'class' => CompanyBehavior::class
            ]
        ];
    }

    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }
}