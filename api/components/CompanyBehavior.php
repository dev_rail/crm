<?php

namespace api\components;


use yii\base\Behavior;
use yii\base\Event;
use yii\db\ActiveRecord;
use yii\web\ForbiddenHttpException;

class CompanyBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'updateRecord',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'updateRecord',
            ActiveRecord::EVENT_INIT => 'updateRecord'
        ];
    }

    public function updateRecord(Event $event)
    {
        if (!\Yii::$app->user->identity)
            return false;

        $model = $event->sender;
        $company_id = \Yii::$app->user->identity->company_id;

        if (!$model->company_id) {
            $model->company_id = $company_id;
        }
    }
}