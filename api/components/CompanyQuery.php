<?php

namespace api\components;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

class CompanyQuery extends ActiveQuery
{
    private $company_ids = [];

    public function createCommand($db = null)
    {
        if (\Yii::$app->user->identity) {
            $company_id = \Yii::$app->user->identity->company_id;
            $ids = $this->company_ids;
            $ids[] = $company_id;
            $this->andWhere(['company_id' => $ids]);
        }

        return parent::createCommand($db);
    }

    public function processing()
    {
        if (!\Yii::$app->user->identity)
            return $this;

        $company = \Yii::$app->user->identity->company;
        $ids = $company->processingIds;

        $this->company_ids = ArrayHelper::merge($this->company_ids, $ids);
        return $this;
    }
}