<?php

namespace api\controllers;


use yii\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\Response;

class BaseController extends ActiveController
{
    public function behaviors()
    {
        $behaviors =  parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
        ];

        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
        return $behaviors;
    }

    public function runAction($id, $params=array()){
        $params = array_merge(\Yii::$app->request->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }
}