<?php
/**
 * Created by PhpStorm.
 * User: rail
 * Date: 21.02.19
 * Time: 12:12
 */

namespace api\controllers;


use yii\filters\auth\HttpBearerAuth;
use yii\web\Controller;

class BaseWebController extends Controller
{
    public function behaviors()
    {
        $behaviors =  parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::class,
        ];

        return $behaviors;
    }

    public function runAction($id, $params=array()){
        $params = array_merge(\Yii::$app->request->getBodyParams(), $params);
        return parent::runAction($id, $params);
    }
}