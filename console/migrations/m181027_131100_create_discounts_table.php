<?php

use console\components\Migration;


/**
 * Handles the creation of table `discounts`.
 */
class m181027_131100_create_discounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('discounts', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'partner_id' => $this->integer(),
            'type' => $this->integer(),
            'amount' => $this->decimal(10, 2),
        ]);

        $this->fk('discounts', 'company_id', 'company', 'id');
        $this->fk('discounts', 'partner_id', 'partners', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('discounts');
    }
}
