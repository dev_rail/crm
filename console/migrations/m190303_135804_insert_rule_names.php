<?php

use yii\db\Migration;

/**
 * Class m190303_135804_insert_rule_names
 */
class m190303_135804_insert_rule_names extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $ids[] = 'productgroups-update';
        $ids[] = 'productgroups-delete';
        $ids[] = 'productgroups-create';
        $ids[] = 'products-update';
        $ids[] = 'products-delete';
        $ids[] = 'products-create';
        $ids[] = 'objects-update';
        $ids[] = 'objects-delete';

        $this->update("auth_item", [
           'rule_name' => 'isAuthor'
        ], [
            'name' => $ids
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $ids[] = 'productgroups-update';
        $ids[] = 'productgroups-delete';
        $ids[] = 'productgroups-create';
        $ids[] = 'products-update';
        $ids[] = 'products-delete';
        $ids[] = 'products-create';
        $ids[] = 'objects-update';
        $ids[] = 'objects-delete';

        $this->update("auth_item", [
            'rule_name' => null
        ], [
            'name' => $ids
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190303_135804_insert_rule_names cannot be reverted.\n";

        return false;
    }
    */
}
