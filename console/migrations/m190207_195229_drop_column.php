<?php

use console\components\Migration;


/**
 * Class m190207_195229_drop_column
 */
class m190207_195229_drop_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey("fk-sale_card-sale_id", "sale_card");
        $this->dropForeignKey("fk-sale_items-sale_id", "sale_items");
        $this->dropForeignKey("fk-sale_object-sale_id", "sale_object");

        $this->fk("sale_card", "sale_id", "sales", "id", "CASCADE", "CASCADE", false);
        $this->fk("sale_items", "sale_id", "sales", "id", "CASCADE", "CASCADE", false);
        $this->fk("sale_object", "sale_id", "sales", "id", "CASCADE", "CASCADE", false);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk-sale_card-sale_id", "sale_card");
        $this->dropForeignKey("fk-sale_items-sale_id", "sale_items");
        $this->dropForeignKey("fk-sale_object-sale_id", "sale_object");

        $this->fk("sale_card", "sale_id", "sales", "id", "CASCADE", "CASCADE", false);
        $this->fk("sale_items", "sale_id", "sales", "id", "CASCADE", "CASCADE", false);
        $this->fk("sale_object", "sale_id", "sales", "id", "CASCADE", "CASCADE", false);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190207_195229_drop_column cannot be reverted.\n";

        return false;
    }
    */
}
