<?php

use console\components\Migration;


/**
 * Handles the creation of table `object_products`.
 */
class m181027_132555_create_object_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('object_products', [
            'id' => $this->primaryKey(),
            'object_id' => $this->integer(),
            'product_id' => $this->integer(),
            'price' => $this->decimal(10, 2),
        ]);

        $this->fk('object_products', 'object_id', 'objects', 'id');
        $this->fk('object_products', 'product_id', 'products', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('object_products');
    }
}
