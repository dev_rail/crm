<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale_cash`.
 * Has foreign keys to the tables:
 *
 * - `sales`
 */
class m190308_190201_create_sale_cash_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sale_cash', [
            'id' => $this->primaryKey(),
            'sale_id' => $this->integer(),
            'sum' => $this->decimal(12,2),
        ]);

        // creates index for column `sale_id`
        $this->createIndex(
            'idx-sale_cash-sale_id',
            'sale_cash',
            'sale_id'
        );

        // add foreign key for table `sales`
        $this->addForeignKey(
            'fk-sale_cash-sale_id',
            'sale_cash',
            'sale_id',
            'sales',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `sales`
        $this->dropForeignKey(
            'fk-sale_cash-sale_id',
            'sale_cash'
        );

        // drops index for column `sale_id`
        $this->dropIndex(
            'idx-sale_cash-sale_id',
            'sale_cash'
        );

        $this->dropTable('sale_cash');
    }
}
