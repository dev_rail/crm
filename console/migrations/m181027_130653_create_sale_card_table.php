<?php

use console\components\Migration;


/**
 * Handles the creation of table `sale_card`.
 */
class m181027_130653_create_sale_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sale_card', [
            'id' => $this->primaryKey(),
            'sale_id' => $this->integer(),
            'card_id' => $this->integer(),
        ]);

        $this->fk('sale_card', 'sale_id', 'sales', 'id');
        $this->fk('sale_card', 'card_id', 'cards', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sale_card');
    }
}
