<?php

use yii\db\Migration;

/**
 * Handles the creation of table `users_partners`.
 * Has foreign keys to the tables:
 *
 * - `company_users`
 * - `partners`
 */
class m190223_132240_create_users_partners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('users_partners', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'partner_id' => $this->integer(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-users_partners-user_id',
            'users_partners',
            'user_id'
        );

        // add foreign key for table `company_users`
        $this->addForeignKey(
            'fk-users_partners-user_id',
            'users_partners',
            'user_id',
            'company_users',
            'id',
            'CASCADE'
        );

        // creates index for column `partner_id`
        $this->createIndex(
            'idx-users_partners-partner_id',
            'users_partners',
            'partner_id'
        );

        // add foreign key for table `partners`
        $this->addForeignKey(
            'fk-users_partners-partner_id',
            'users_partners',
            'partner_id',
            'partners',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `company_users`
        $this->dropForeignKey(
            'fk-users_partners-user_id',
            'users_partners'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-users_partners-user_id',
            'users_partners'
        );

        // drops foreign key for table `partners`
        $this->dropForeignKey(
            'fk-users_partners-partner_id',
            'users_partners'
        );

        // drops index for column `partner_id`
        $this->dropIndex(
            'idx-users_partners-partner_id',
            'users_partners'
        );

        $this->dropTable('users_partners');
    }
}
