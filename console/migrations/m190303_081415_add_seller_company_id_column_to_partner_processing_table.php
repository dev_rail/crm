<?php

use yii\db\Migration;

/**
 * Handles adding seller_company_id to table `partner_processing`.
 * Has foreign keys to the tables:
 *
 * - `company`
 */
class m190303_081415_add_seller_company_id_column_to_partner_processing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('partner_processing', 'seller_company_id', $this->integer());

        // creates index for column `seller_company_id`
        $this->createIndex(
            'idx-partner_processing-seller_company_id',
            'partner_processing',
            'seller_company_id'
        );

        // add foreign key for table `company`
        $this->addForeignKey(
            'fk-partner_processing-seller_company_id',
            'partner_processing',
            'seller_company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `company`
        $this->dropForeignKey(
            'fk-partner_processing-seller_company_id',
            'partner_processing'
        );

        // drops index for column `seller_company_id`
        $this->dropIndex(
            'idx-partner_processing-seller_company_id',
            'partner_processing'
        );

        $this->dropColumn('partner_processing', 'seller_company_id');
    }
}
