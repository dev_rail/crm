<?php

use yii\db\Migration;

/**
 * Handles adding actual_price to table `sale_items`.
 */
class m190309_064144_add_actual_price_column_to_sale_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sale_items', 'actual_price', $this->decimal(10,2));
        $this->addColumn('sale_items', 'actual_sum', $this->decimal(10,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sale_items', 'actual_price');
        $this->dropColumn('sale_items', 'actual_sum');
    }
}
