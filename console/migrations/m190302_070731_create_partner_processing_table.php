<?php

use yii\db\Migration;

/**
 * Handles the creation of table `partner_processing`.
 * Has foreign keys to the tables:
 *
 * - `partners`
 * - `company`
 */
class m190302_070731_create_partner_processing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('partner_processing', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer(),
            'company_id' => $this->integer(),
        ]);

        // creates index for column `partner_id`
        $this->createIndex(
            'idx-partner_processing-partner_id',
            'partner_processing',
            'partner_id'
        );

        // add foreign key for table `partners`
        $this->addForeignKey(
            'fk-partner_processing-partner_id',
            'partner_processing',
            'partner_id',
            'partners',
            'id',
            'CASCADE'
        );

        // creates index for column `company_id`
        $this->createIndex(
            'idx-partner_processing-company_id',
            'partner_processing',
            'company_id'
        );

        // add foreign key for table `company`
        $this->addForeignKey(
            'fk-partner_processing-company_id',
            'partner_processing',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `partners`
        $this->dropForeignKey(
            'fk-partner_processing-partner_id',
            'partner_processing'
        );

        // drops index for column `partner_id`
        $this->dropIndex(
            'idx-partner_processing-partner_id',
            'partner_processing'
        );

        // drops foreign key for table `company`
        $this->dropForeignKey(
            'fk-partner_processing-company_id',
            'partner_processing'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            'idx-partner_processing-company_id',
            'partner_processing'
        );

        $this->dropTable('partner_processing');
    }
}
