<?php

use console\components\Migration;


/**
 * Handles the creation of table `cards`.
 */
class m181027_122710_create_cards_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('cards', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'partner_id' => $this->integer(),
            'number' => $this->integer(),
            'uid' => $this->string(),
            'name' => $this->string(),
        ]);

        $this->fk('cards', 'company_id', 'company', 'id');
        $this->fk('cards', 'partner_id', 'partners', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('cards');
    }
}
