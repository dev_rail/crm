<?php

use yii\db\Migration;

/**
 * Handles adding product_id to table `discounts`.
 * Has foreign keys to the tables:
 *
 * - `products`
 */
class m190301_030002_add_product_id_column_to_discounts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('discounts', 'product_id', $this->integer());

        // creates index for column `product_id`
        $this->createIndex(
            'idx-discounts-product_id',
            'discounts',
            'product_id'
        );

        // add foreign key for table `products`
        $this->addForeignKey(
            'fk-discounts-product_id',
            'discounts',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `products`
        $this->dropForeignKey(
            'fk-discounts-product_id',
            'discounts'
        );

        // drops index for column `product_id`
        $this->dropIndex(
            'idx-discounts-product_id',
            'discounts'
        );

        $this->dropColumn('discounts', 'product_id');
    }
}
