<?php

use console\components\Migration;

/**
 * Handles the creation of table `product_groups`.
 */
class m181027_121703_create_product_groups_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product_groups', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'parent_id' => $this->integer(),
            'name' => $this->string(),
        ]);

        $this->fk('product_groups', 'company_id', 'company', 'id');
        $this->fk('product_groups', 'parent_id', 'product_groups', 'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('product_groups');
    }
}
