<?php

use console\components\Migration;


/**
 * Handles the creation of table `discount_cards`.
 */
class m181027_132037_create_discount_cards_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('discount_cards', [
            'id' => $this->primaryKey(),
            'discount_id' => $this->integer(),
            'card_id' => $this->integer(),
            'type' => $this->integer(),
            'amount' => $this->decimal(10, 2),
        ]);

        $this->fk('discount_cards', 'discount_id', 'discounts', 'id');
        $this->fk('discount_cards', 'card_id', 'cards', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('discount_cards');
    }
}
