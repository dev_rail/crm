<?php

use console\components\Migration;


/**
 * Handles the creation of table `company_users`.
 */
class m181027_124015_create_company_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('company_users', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'username' => $this->string(),
            'password' => $this->string(),
        ]);

        $this->fk('company_users', 'company_id', 'company', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('company_users');
    }
}
