<?php

use yii\db\Migration;

/**
 * Handles adding bonus to table `sale_card`.
 */
class m190101_054151_add_bonus_column_to_sale_card_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sale_card', 'bonus', $this->decimal(10,2));
        $this->addColumn('sale_card', 'bonus_percent', $this->decimal(10,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sale_card', 'bonus');
        $this->dropColumn('sale_card', 'bonus_percent');
    }
}
