<?php

/**
 * Handles the creation of table `partners`.
 */
class m181027_113639_create_partners_table extends \console\components\Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('partners', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'name' => $this->string(),
            'full_name' => $this->string(),
            'inn' => $this->string(),
            'address' => $this->string(),
            'balance' => $this->decimal(),
        ]);

        $this->fk('partners', 'company_id', 'company', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('partners');
    }
}
