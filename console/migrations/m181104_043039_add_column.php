<?php

use yii\db\Migration;

/**
 * Class m181104_043039_add_column
 */
class m181104_043039_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn("objects", "position");
        $this->addColumn("objects", "address", $this->string());
        $this->addColumn("objects", "lat", $this->string());
        $this->addColumn("objects", "lng", $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->addColumn("objects", "position", $this->json());
        $this->dropColumn("objects", "address");
        $this->dropColumn("objects", "lat");
        $this->dropColumn("objects", "lng");
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181104_043039_add_column cannot be reverted.\n";

        return false;
    }
    */
}
