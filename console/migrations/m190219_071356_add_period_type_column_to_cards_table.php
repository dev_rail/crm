<?php

use yii\db\Migration;

/**
 * Handles adding period_type to table `cards`.
 */
class m190219_071356_add_period_type_column_to_cards_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cards', 'period_type', $this->integer()->defaultValue(0));
        $this->addColumn('cards', 'limit_value', $this->decimal(10,2));
        $this->addColumn('cards', 'limit_type', $this->integer()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('cards', 'period_type');
        $this->dropColumn('cards', 'limit_value');
        $this->dropColumn('cards', 'limit_type');
    }
}
