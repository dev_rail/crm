<?php

use yii\db\Migration;

/**
 * Handles the creation of table `not_reg_cards`.
 * Has foreign keys to the tables:
 *
 * - `objects`
 * - `terminals`
 */
class m181209_082109_create_not_reg_cards_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('not_reg_cards', [
            'id' => $this->primaryKey(),
            'date' => $this->integer(),
            'uid' => $this->string(),
            'object_id' => $this->integer(),
            'terminal_id' => $this->integer(),
            'company_id' => $this->integer()
        ]);

        // creates index for column `object_id`
        $this->createIndex(
            'idx-not_reg_cards-object_id',
            'not_reg_cards',
            'object_id'
        );

        // add foreign key for table `objects`
        $this->addForeignKey(
            'fk-not_reg_cards-object_id',
            'not_reg_cards',
            'object_id',
            'objects',
            'id',
            'CASCADE'
        );

        // creates index for column `terminal_id`
        $this->createIndex(
            'idx-not_reg_cards-terminal_id',
            'not_reg_cards',
            'terminal_id'
        );

        // add foreign key for table `terminals`
        $this->addForeignKey(
            'fk-not_reg_cards-terminal_id',
            'not_reg_cards',
            'terminal_id',
            'terminals',
            'id',
            'CASCADE'
        );


        // creates index for column `terminal_id`
        $this->createIndex(
            'idx-not_reg_cards-company_id',
            'not_reg_cards',
            'company_id'
        );

        // add foreign key for table `terminals`
        $this->addForeignKey(
            'fk-not_reg_cards-company_id',
            'not_reg_cards',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `objects`
        $this->dropForeignKey(
            'fk-not_reg_cards-object_id',
            'not_reg_cards'
        );

        // drops index for column `object_id`
        $this->dropIndex(
            'idx-not_reg_cards-object_id',
            'not_reg_cards'
        );

        // drops foreign key for table `terminals`
        $this->dropForeignKey(
            'fk-not_reg_cards-terminal_id',
            'not_reg_cards'
        );

        // drops index for column `terminal_id`
        $this->dropIndex(
            'idx-not_reg_cards-terminal_id',
            'not_reg_cards'
        );

        // drops index for column `terminal_id`
        $this->dropIndex(
            'idx-not_reg_cards-company_id',
            'not_reg_cards'
        );

        $this->dropTable('not_reg_cards');
    }
}
