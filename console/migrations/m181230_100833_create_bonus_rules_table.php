<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bonus_rules`.
 * Has foreign keys to the tables:
 *
 * - `partners`
 */
class m181230_100833_create_bonus_rules_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('bonus_rules', [
            'id' => $this->primaryKey(),
            'partner_id' => $this->integer(),
            'percent' => $this->decimal(10,2),
        ]);

        // creates index for column `partner_id`
        $this->createIndex(
            'idx-bonus_rules-partner_id',
            'bonus_rules',
            'partner_id'
        );

        // add foreign key for table `partners`
        $this->addForeignKey(
            'fk-bonus_rules-partner_id',
            'bonus_rules',
            'partner_id',
            'partners',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `partners`
        $this->dropForeignKey(
            'fk-bonus_rules-partner_id',
            'bonus_rules'
        );

        // drops index for column `partner_id`
        $this->dropIndex(
            'idx-bonus_rules-partner_id',
            'bonus_rules'
        );

        $this->dropTable('bonus_rules');
    }
}
