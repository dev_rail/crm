<?php

use console\components\Migration;


/**
 * Handles the creation of table `terminals`.
 */
class m181027_121216_create_terminals_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('terminals', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'object_id' => $this->integer(),
            'imei' => $this->string(),
        ]);

        $this->fk('terminals', 'company_id', 'company', 'id');
        $this->fk('terminals', 'object_id', 'objects', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('terminals');
    }
}
