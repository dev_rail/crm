<?php

use yii\db\Migration;

/**
 * Handles adding limit to table `partners`.
 */
class m181212_040930_add_limit_column_to_partners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('partners', 'limit', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('partners', 'limit');
    }
}
