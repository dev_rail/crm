<?php

use yii\db\Migration;

/**
 * Class m181104_121434_alter_column
 */
class m181104_121434_alter_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('sales', 'sum', $this->decimal(10, 2)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('sales', 'sum', $this->decimal(10, 2));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181104_121434_alter_column cannot be reverted.\n";

        return false;
    }
    */
}
