<?php

use console\components\Migration;


/**
 * Handles the creation of table `products`.
 */
class m181027_122202_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('products', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'group_id' => $this->integer(),
            'name' => $this->string(),
            'price' => $this->decimal(10, 2),
        ]);

        $this->fk('products', 'company_id', 'company', 'id');
        $this->fk('products', 'group_id', 'product_groups', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('products');
    }
}
