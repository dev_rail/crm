<?php

use console\components\Migration;

/**
 * Handles the creation of table `discount_objects`.
 */
class m181027_131452_create_discount_objects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('discount_objects', [
            'id' => $this->primaryKey(),
            'discount_id' => $this->integer(),
            'object_id' => $this->integer(),
            'type' => $this->integer(),
            'amount' => $this->decimal(10, 2),
        ]);

        $this->fk('discount_objects', 'discount_id', 'discounts', 'id',
            'CASCADE', 'CASCADE');

        $this->fk('discount_objects', 'object_id', 'objects', 'id',
            'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('discount_objects');
    }
}
