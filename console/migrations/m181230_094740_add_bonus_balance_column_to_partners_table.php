<?php

use yii\db\Migration;

/**
 * Handles adding bonus_balance to table `partners`.
 */
class m181230_094740_add_bonus_balance_column_to_partners_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('cards', 'bonus_balance', $this->decimal(10,2));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('cards', 'bonus_balance');
    }
}
