<?php

use console\components\Migration;


/**
 * Handles the creation of table `terminal_sessions`.
 */
class m181027_124448_create_terminal_sessions_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('terminal_sessions', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'object_id' => $this->integer(),
            'terminal_id' => $this->integer(),
            'user_id' => $this->integer(),
            'created_at' => $this->integer(),
            'closed_at' => $this->integer()->defaultValue(null),
            'token' => $this->string(),
            'close' => $this->boolean()->defaultValue(false),
        ]);

        $this->fk('terminal_sessions', 'company_id', 'company', 'id');
        $this->fk('terminal_sessions', 'object_id', 'objects', 'id');
        $this->fk('terminal_sessions', 'terminal_id', 'terminals', 'id');
        $this->fk('terminal_sessions', 'user_id', 'company_users', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('terminal_sessions');
    }
}
