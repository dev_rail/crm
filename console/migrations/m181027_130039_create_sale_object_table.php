<?php

use console\components\Migration;


/**
 * Handles the creation of table `sale_object`.
 */
class m181027_130039_create_sale_object_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sale_object', [
            'id' => $this->primaryKey(),
            'sale_id' => $this->integer(),
            'object_id' => $this->integer(),
            'terminal_id' => $this->integer(),
            'user_id' => $this->integer(),
            'session_id' => $this->integer(),
        ]);

        $this->fk('sale_object', 'sale_id', 'sales', 'id', 'CASCADE', 'CASCADE');
        $this->fk('sale_object', 'object_id', 'objects', 'id');
        $this->fk('sale_object', 'terminal_id', 'terminals', 'id');
        $this->fk('sale_object', 'user_id', 'company_users', 'id');
        $this->fk('sale_object', 'session_id', 'terminal_sessions', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sale_object');
    }
}
