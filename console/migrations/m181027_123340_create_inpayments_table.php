<?php

use console\components\Migration;


/**
 * Handles the creation of table `inpayments`.
 */
class m181027_123340_create_inpayments_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('inpayments', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'partner_id' => $this->integer(),
            'date' => $this->integer(),
            'sum' => $this->decimal(13, 2),
            'description' => $this->string()->defaultValue(null)
        ]);

        $this->fk('inpayments', 'company_id', 'company', 'id');
        $this->fk('inpayments', 'partner_id', 'partners', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('inpayments');
    }
}
