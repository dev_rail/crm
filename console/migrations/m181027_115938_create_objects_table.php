<?php

use console\components\Migration;


/**
 * Handles the creation of table `objects`.
 */
class m181027_115938_create_objects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('objects', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'name' => $this->string(),
            'position' => $this->json(),
        ]);

        $this->fk('objects', 'company_id', 'company', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('objects');
    }
}
