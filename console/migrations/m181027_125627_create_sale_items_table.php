<?php

use console\components\Migration;


/**
 * Handles the creation of table `sale_items`.
 */
class m181027_125627_create_sale_items_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sale_items', [
            'id' => $this->primaryKey(),
            'sale_id' => $this->integer(),
            'product_id' => $this->integer(),
            'price' => $this->decimal(10, 2),
            'count' => $this->decimal(10, 2),
            'sum' => $this->decimal(13, 2),
        ]);

        $this->fk('sale_items', 'sale_id', 'sales', 'id', "CASCADE", "CASCADE");
        $this->fk('sale_items', 'product_id', 'products', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sale_items');
    }
}
