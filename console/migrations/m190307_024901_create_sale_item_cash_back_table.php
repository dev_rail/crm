<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sale_item_cash_back`.
 * Has foreign keys to the tables:
 *
 * - `sale_items`
 */
class m190307_024901_create_sale_item_cash_back_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sale_item_cash_back', [
            'id' => $this->primaryKey(),
            'sale_item_id' => $this->integer(),
            'price' => $this->decimal(10,2),
            'diff' => $this->decimal(10,2),
            'sum' => $this->decimal(10,2),
        ]);

        // creates index for column `sale_item_id`
        $this->createIndex(
            'idx-sale_item_cash_back-sale_item_id',
            'sale_item_cash_back',
            'sale_item_id'
        );

        // add foreign key for table `sale_items`
        $this->addForeignKey(
            'fk-sale_item_cash_back-sale_item_id',
            'sale_item_cash_back',
            'sale_item_id',
            'sale_items',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `sale_items`
        $this->dropForeignKey(
            'fk-sale_item_cash_back-sale_item_id',
            'sale_item_cash_back'
        );

        // drops index for column `sale_item_id`
        $this->dropIndex(
            'idx-sale_item_cash_back-sale_item_id',
            'sale_item_cash_back'
        );

        $this->dropTable('sale_item_cash_back');
    }
}
