<?php

use console\components\Migration;

/**
 * Handles the creation of table `sales`.
 */
class m181027_125054_create_sales_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('sales', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'partner_id' => $this->integer(),
            'date' => $this->integer(),
            'sum' => $this->decimal(13, 2),
        ]);

        $this->fk('sales', 'company_id', 'company', 'id');
        $this->fk('sales', 'partner_id', 'partners', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('sales');
    }
}
