<?php

use yii\db\Migration;

/**
 * Handles adding type_pay to table `sales`.
 */
class m181230_083511_add_type_pay_column_to_sales_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('sales', 'type_pay', $this->integer()->defaultValue(1));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('sales', 'type_pay');
    }
}
