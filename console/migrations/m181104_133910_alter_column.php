<?php

use yii\db\Migration;

/**
 * Class m181104_133910_alter_column
 */
class m181104_133910_alter_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('sale_items', 'sum', $this->decimal(10, 2)->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('sale_items', 'sum', $this->decimal(10, 2));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181104_133910_alter_column cannot be reverted.\n";

        return false;
    }
    */
}
