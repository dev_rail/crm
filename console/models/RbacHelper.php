<?php

namespace console\models;


use Yii;
use yii\base\Model;
use yii\rbac\DbManager;

class RbacHelper extends Model
{
    const ROLE_ADMIN_NAME = 'admin';

    /**
     * @var DbManager
     */
    private $auth;

    public function init()
    {
        parent::init();
        $this->auth = Yii::$app->authManager;
    }

    public function addRole($name, $description, $defaultRoute)
    {
        $auth = $this->auth;
        $route = $defaultRoute;

        $role = $auth->getRole($name);
        if ($role) {
            if ($role->data["defaultRoute"] != $route)
                $role->data["defaultRoute"] = $route;

            if ($role->description != $description)
                $role->description = $description;

            return $role;
        }

        $role = $auth->createRole($name);
        $role->description = $description;
        $role->data["defaultRoute"] = $route;
        $auth->add($role);

        if ($name == 'audit')
        {
            $admin = $auth->getRole('admin');
            $auth->addChild($role, $admin);
        }

        return $role;
    }

    public function addPermissionsByControllerName($name)
    {
        $u_name = mb_strtolower($name);
        $controllerName = "frontend\controllers\\".$name."Controller";

        $auth = $this->auth;
        $admin = $auth->getRole(self::ROLE_ADMIN_NAME);

        $controller = new $controllerName($u_name, "");

        $result = [];
        $permissions = $controller->permissions;
        foreach ($permissions as $permission => $description)
        {
            $p = $auth->getPermission($permission);

            if (!$p) {

                $p = $auth->createPermission($permission);
                $p->description = $description;
                $auth->add($p);

                $auth->addChild($admin, $p);
                $result[] = $p->name;
            }
        }

        return $result;
    }
}