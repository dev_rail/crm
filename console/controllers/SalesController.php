<?php

namespace console\controllers;

use common\models\sales\SaleItems;
use common\models\sales\Sales;
use yii\console\Controller;

class SalesController extends Controller
{
    public function actionUpdateSaleItems()
    {
        $items = SaleItems::find();

        foreach ($items->each() as $item)
        {
            $item->actual_price = $item->price;
            $item->save();
            echo $item->id."\r\n";
        }
    }
}