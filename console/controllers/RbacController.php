<?php

namespace console\controllers;


use common\models\rbac\AuthItem;
use console\models\RbacHelper;
use frontend\controllers\CardsController;
use frontend\rbac\AuthorRule;
use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    /**
     * Инициализация ролей
     * @throws \Exception
     */
    public function actionInit()
    {
        $rbac_helper = new RbacHelper();

        $role = $rbac_helper->addRole('admin', 'Администратор', '/partners');
        echo "Роль: ".$role->name."(".$role->description.") - инициализированна \r\n";

        $role = $rbac_helper->addRole('operator', 'Оператор', '/sales/index');
        echo "Роль: ".$role->name."(".$role->description.") - инициализированна \r\n";

        $role = $rbac_helper->addRole('audit', 'Аудит', '/partners');
        echo "Роль: ".$role->name."(".$role->description.") - инициализированна \r\n";

        $role = $rbac_helper->addRole("client", 'Клиент', "/sales");
        echo "Роль: ".$role->name."(".$role->description.") - инициализированна \r\n";
    }



    /**
     * Добавление разрешений по имени контроллера
     * @param $name
     * @throws \yii\base\Exception
     */
    public function actionController($name)
    {
        $rbac_helper = new RbacHelper();
        $permissions = $rbac_helper->addPermissionsByControllerName($name);

        echo "Для контроллера $name добавлены следующие разрешения: \r\n";
        foreach ($permissions as $permission)
            echo $permission."\r\n";
    }


    /**
     * Добовляет разрешение для роли
     * @param $permission
     * @param $role_name
     * @throws \yii\base\Exception
     */
    public function actionAddPermission($permission, $role_name)
    {
        $auth = Yii::$app->authManager;

        $role = $auth->getRole($role_name);
        $p = $auth->getPermission($permission);

        if ($role && $p)
            $auth->addChild($role, $p);
    }


    /**
     * Устанавливает роль на пользователя
     * @param $user_id
     * @param $role_name
     * @throws \Exception
     */
    public function actionSetRole($user_id, $role_name)
    {
        $auth = Yii::$app->authManager;

        $auth->revokeAll($user_id);

        $role = $auth->getRole($role_name);
        if ($role)
            $auth->assign($role, $user_id);
    }

    public function actionAddAuthorRule()
    {
        $auth = Yii::$app->authManager;

        $rule = $auth->getRule('isAuthor');
        if (!$rule) {
            $rule = new AuthorRule();

            $auth->add($rule);
        }

        echo "success";
    }
}
