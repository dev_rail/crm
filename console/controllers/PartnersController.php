<?php

namespace console\controllers;


use common\models\partners\PartnerProcessing;
use yii\console\Controller;

class PartnersController extends Controller
{
    public function actionSetProcessing($partner_id, $company_id)
    {
        $process = new PartnerProcessing([
            'company_id' => $company_id,
            'partner_id' => $partner_id
        ]);

        $process->save();

        echo "Success \r\n";
    }
}