<?php

namespace console\controllers;


use common\models\CompanyUsers;
use Yii;
use yii\console\Controller;

class UserController extends Controller
{
    /**
     * Добавление пользователя
     * @param $company_id
     * @param $username
     * @param $password
     * @return string
     */
    public function actionAdd($company_id, $username, $password)
    {
        $user = new CompanyUsers([
            'company_id' => $company_id,
            'username' => $username
        ]);

        $user->setPassword($password);

        $user->save();

        return "Success";
    }
}