<?php
namespace console\controllers;


use common\models\discounts\DiscountCards;
use yii\console\Controller;

class TestController extends Controller
{
    public function actionIndex()
    {
        $discount_card = DiscountCards::findOne(7);
        echo $discount_card->price;
    }
}