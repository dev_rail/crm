<?php

namespace console\components;


class Migration extends \yii\db\Migration
{
    public function fk($table, $field, $table_ref, $field_ref, $delete = null, $update = null, $index = true)
    {
        if ($index)
            $this->createIndex('idx-'.$table."-".$field, $table, $field);

        $this->addForeignKey('fk-'.$table."-".$field, $table, $field,
            $table_ref, $field_ref, $delete, $update);
    }
}