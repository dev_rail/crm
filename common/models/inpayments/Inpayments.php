<?php

namespace common\models\inpayments;

use common\models\Company;
use common\models\partners\Partners;
use Yii;

/**
 * This is the model class for table "inpayments".
 *
 * @property int $id
 * @property int $company_id
 * @property int $partner_id
 * @property int $date
 * @property string $sum
 * @property string $description
 *
 * @property Company $company
 * @property Partners $partner
 */
class Inpayments extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inpayments';
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INSERT, [Partners::class, 'updateBalance']);
        $this->on(self::EVENT_AFTER_UPDATE, [Partners::class, 'updateBalance']);
        $this->on(self::EVENT_AFTER_DELETE, [Partners::class, 'updateBalance']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'partner_id', 'date', 'sum'], 'required'],
            [['company_id', 'partner_id', 'date'], 'integer'],
            [['sum'], 'number'],
            [['description'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'partner_id' => 'Контрагент',
            'date' => 'Дата',
            'sum' => 'Сумма',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }
}
