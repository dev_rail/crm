<?php

namespace common\models;

use common\models\discounts\Discounts;
use common\models\inpayments\Inpayments;
use common\models\objects\Objects;
use common\models\objects\Terminals;
use common\models\objects\TerminalSessions;
use common\models\partners\Cards;
use common\models\partners\Partners;
use common\models\products\ProductGroups;
use common\models\products\Products;
use common\models\sales\Sales;
use Yii;

/**
 * This is the model class for table "company".
 *
 * @property int $id
 * @property string $name
 *
 * @property Cards[] $cards
 * @property CompanyUsers[] $companyUsers
 * @property Discounts[] $discounts
 * @property Inpayments[] $inpayments
 * @property Objects[] $objects
 * @property Partners[] $partners
 * @property ProductGroups[] $productGroups
 * @property Products[] $products
 * @property Sales[] $sales
 * @property TerminalSessions[] $terminalSessions
 * @property Terminals[] $terminals
 */
class Company extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(Cards::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyUsers()
    {
        return $this->hasMany(CompanyUsers::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(Discounts::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInpayments()
    {
        return $this->hasMany(Inpayments::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjects()
    {
        return $this->hasMany(Objects::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartners()
    {
        return $this->hasMany(Partners::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroups()
    {
        return $this->hasMany(ProductGroups::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasMany(Sales::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminalSessions()
    {
        return $this->hasMany(TerminalSessions::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminals()
    {
        return $this->hasMany(Terminals::className(), ['company_id' => 'id']);
    }
}
