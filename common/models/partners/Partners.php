<?php

namespace common\models\partners;

use common\models\Company;
use common\models\discounts\Discounts;
use common\models\inpayments\Inpayments;
use common\models\sales\Sales;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "partners".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $full_name
 * @property string $inn
 * @property string $address
 * @property float $balance
 * @property integer $limit
 *
 * @property Cards[] $cards
 * @property Discounts[] $discounts
 * @property Inpayments[] $inpayments
 * @property Company $company
 * @property Sales[] $sales
 * @property float $enabledBalanceSum
 */
class Partners extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'limit'], 'integer'],
            [['balance', 'bonus'], 'number'],
            [['limit'], 'default', 'value' => 0],
            [['name', 'full_name', 'inn', 'address'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['inn', 'company_id'], 'unique', 'targetAttribute' => ['inn', 'company_id'], 'message' => 'Контрагент с данным ИНН уже существует!']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Наименование',
            'full_name' => 'Полное наименование',
            'inn' => 'ИНН',
            'address' => 'Адрес',
            'balance' => 'Баланс',
            'limit' => 'Лимит',
            'enabledBalanceSum' => 'Доступный остаток',
            'bonus' => 'Бонус'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCards()
    {
        return $this->hasMany(Cards::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(Discounts::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInpayments()
    {
        return $this->hasMany(Inpayments::className(), ['partner_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSales()
    {
        return $this->hasMany(Sales::className(), ['partner_id' => 'id']);
    }

    public function updateBalance(Event $event = null)
    {
        if ($event)
            $model = $event->sender->partner;
        else
            $model = $this;

        $sum_inpayments = $model->getInpayments()->sum('sum');
        $sum_sales = $model->getSales()->where(['type_pay' => Sales::TYPE_PAY_ELECTRON])->sum('sum');

        $model->balance = $sum_inpayments - $sum_sales;
        $model->save();
    }

    /**
     * Возвращает доступный баланс (баланс + лимит)
     * @return string
     */
    public function getEnabledBalanceSum()
    {
        return $this->balance + $this->limit;
    }

    public function getBonusRule()
    {
        return $this->hasOne(BonusRules::class, ['partner_id' => 'id']);
    }

    public function getBonus()
    {
        return ArrayHelper::getValue($this, "bonusRule.percent", 0);
    }

    public function setBonus($percent)
    {
        $model = $this->bonusRule;
        if (!$model)
        {
            $model = new BonusRules([
                'partner_id' => $this->id
            ]);
        }

        $model->percent = $percent;
        $model->save();
    }

    public function getProcessing()
    {
        return $this->hasOne(PartnerProcessing::class, ['partner_id' => 'id']);
    }
}
