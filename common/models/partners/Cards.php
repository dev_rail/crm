<?php

namespace common\models\partners;

use common\models\Company;
use common\models\discounts\DiscountCards;
use common\models\sales\SaleCard;
use common\models\sales\SaleItems;
use common\models\sales\Sales;
use phpDocumentor\Reflection\Types\Integer;
use Yii;
use yii\base\Event;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "cards".
 *
 * @property int $id
 * @property int $company_id
 * @property int $partner_id
 * @property int $number
 * @property string $uid
 * @property string $name
 * @property string $bonus_balance
 * @property integer $period_type
 * @property float $limit_value
 * @property integer $limit_type
 *
 * @property Company $company
 * @property Partners $partner
 * @property DiscountCards[] $discountCards
 * @property SaleCard[] $saleCards
 */
class Cards extends \yii\db\ActiveRecord
{
    //Дневной тип периода
    const PERIOD_TYPE_DAY = 1;

    //Недельный тип периода
    const PERIOD_TYPE_WEEK = 2;

    //Месячный тип периода
    const PERIOD_TYPE_MONTH = 3;

    //Количественный тип лимита
    const LIMIT_TYPE_COUNT = 1;

    //Денежный тип лимита
    const LIMIT_TYPE_MONEY = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'partner_id', 'number', 'period_type', 'limit_type'], 'integer'],
            [['uid', 'name'], 'string', 'max' => 255],
            [['bonus_balance', 'limit_value'], 'number'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
            ['uid', 'unique', 'message' => 'Карта уже добавлена в систему!']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'partner_id' => 'Partner ID',
            'number' => 'Номер',
            'uid' => 'Электронный номер',
            'name' => 'Наименование',
            'bonus_balance' => 'Бонусы',
            'period_type' => 'Период лимита',
            'limit_type' => 'Тип лимита',
            'limit_value' => 'Значение лимита',
            'periodTypeText' => 'Период лимита',
            'limitTypeText' => 'Тип лимита'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCards()
    {
        return $this->hasMany(DiscountCards::className(), ['card_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleCards()
    {
        return $this->hasMany(SaleCard::className(), ['card_id' => 'id']);
    }

    public function getBonusPercent()
    {
        return $this->partner->bonus;
    }

    public function updateBonusBalance(Event $event = null)
    {
        if ($event)
        {
            $saleCard = $event->sender;
            $model = $saleCard->card;
        }
        else
            $model = $this;

        $sum_inpayments = $model->getSaleCards()->sum('bonus');
        $sum_sales = Sales::find()->joinWith('saleCard')->where(['card_id' => $model->id])
            ->andWhere(['type_pay' => Sales::TYPE_PAY_BONUS])
            ->sum('sum');

        $model->bonus_balance = $sum_inpayments - $sum_sales;
        $model->save();
    }

    public static function getAllTypePeriods()
    {
        $result = [
            'Нет',
            static::PERIOD_TYPE_DAY => 'Дневной',
            static::PERIOD_TYPE_WEEK => 'Недельный',
            static::PERIOD_TYPE_MONTH => 'Месячный'
        ];

        return $result;
    }

    public static function getAllTypeLimits()
    {
        $result = [
            'Нет',
            static::LIMIT_TYPE_COUNT => 'Количественный',
            static::LIMIT_TYPE_MONEY => 'Денежный'
        ];

        return $result;
    }

    public function getPeriodTypeText()
    {
        $array = static::getAllTypePeriods();

        return ArrayHelper::getValue($array, $this->period_type);
    }

    public function getLimitTypeText()
    {
        $array = static::getAllTypeLimits();

        return ArrayHelper::getValue($array, $this->limit_type);
    }

    public function getSaleItemsCountByPeriod($start, $end)
    {
        $sale_items = SaleItems::find()->joinWith('sale')
            ->joinWith('sale.saleCard')
            ->where(['>=', 'date', $start])
            ->andWhere(['<=', 'date', $end])
            ->andWhere(['sale_card.card_id' => $this->id]);

        return $sale_items->sum('sale_items.count');
    }

    public function getSaleItemsSumByPeriod($start, $end)
    {
        $sale_items = SaleItems::find()->joinWith('sale')
            ->joinWith('sale.saleCard')
            ->where(['>=', 'date', $start])
            ->andWhere(['<=', 'date', $end])
            ->andWhere(['sale_card.card_id' => $this->id]);

        return $sale_items->sum('sale_items.sum');
    }
}
