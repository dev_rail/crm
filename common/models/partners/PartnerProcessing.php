<?php

namespace common\models\partners;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "partner_processing".
 *
 * @property int $id
 * @property int $partner_id
 * @property int $company_id
 *
 * @property Company $company
 * @property Company $sellerCompany
 * @property Partners $partner
 * @property int $seller_company_id
 */
class PartnerProcessing extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner_processing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['partner_id', 'company_id', 'seller_company_id'], 'integer'],
            [['seller_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['seller_company_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['partner_id', 'company_id'], 'unique', 'targetAttribute' => ['partner_id', 'company_id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_id' => 'Partner ID',
            'company_id' => 'Company ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellerCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'seller_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    public function beforeSave($insert)
    {
        $this->updateSellerCompany();
        return parent::beforeSave($insert);
    }

    private function updateSellerCompany()
    {
        $this->seller_company_id = $this->partner->company_id;
    }
}
