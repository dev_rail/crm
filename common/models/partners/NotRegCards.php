<?php

namespace common\models\partners;

use common\models\Company;
use common\models\objects\Objects;
use common\models\objects\Terminals;
use Yii;

/**
 * This is the model class for table "not_reg_cards".
 *
 * @property int $id
 * @property int $date
 * @property string $uid
 * @property int $object_id
 * @property int $terminal_id
 * @property int $company_id
 *
 * @property Company $company
 * @property Objects $object
 * @property Terminals $terminal
 */
class NotRegCards extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'not_reg_cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'object_id', 'terminal_id', 'company_id'], 'integer'],
            [['uid'], 'string', 'max' => 255],
            [['uid', 'object_id'], 'unique', 'targetAttribute' => ['uid', 'object_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['terminal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Terminals::className(), 'targetAttribute' => ['terminal_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'uid' => 'Uid',
            'object_id' => 'Object ID',
            'terminal_id' => 'Terminal ID',
            'company_id' => 'Company ID',
            'object.name' => 'Объект'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminal()
    {
        return $this->hasOne(Terminals::className(), ['id' => 'terminal_id']);
    }
}
