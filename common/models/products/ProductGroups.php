<?php

namespace common\models\products;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "product_groups".
 *
 * @property int $id
 * @property int $company_id
 * @property int $parent_id
 * @property string $name
 *
 * @property Company $company
 * @property ProductGroups $parent
 * @property ProductGroups[] $productGroups
 * @property Products[] $products
 */
class ProductGroups extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_groups';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['company_id', 'required'],
            [['company_id', 'parent_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductGroups::className(), 'targetAttribute' => ['parent_id' => 'id']],
            ['parent_id', 'validateParent']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'parent_id' => 'Категория',
            'name' => 'Наименование',
            'nameText' => 'Наименование',
            'parent.name' => 'Категория'
        ];
    }

    public function validateParent()
    {
        if ($this->parent_id == $this->id)
            $this->addError('parent_id', 'Родитель не может быть тем же объектом!');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(ProductGroups::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroups()
    {
        return $this->hasMany(ProductGroups::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['group_id' => 'id']);
    }

    public function hasProduct()
    {
        if ($this->products)
            return true;

        foreach ($this->productGroups as $group)
        {
            if ($group->hasProduct())
                return true;
        }

        return false;
    }

    public function getNameText()
    {
        $name = $this->name;

        if (!\Yii::$app->user->identity)
            return $name;

        $company = \Yii::$app->user->identity->company;
        if ($company->id != $this->company_id)
        {
            $prefix = $this->company->name;
            $name = "(".$prefix.") ".$name;
        }

        return $name;
    }
}
