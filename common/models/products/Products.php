<?php

namespace common\models\products;

use common\models\Company;
use common\models\discounts\Discounts;
use common\models\objects\ObjectProducts;
use common\models\objects\Objects;
use common\models\partners\Cards;
use common\models\partners\Partners;
use common\models\sales\SaleItems;
use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property int $company_id
 * @property int $group_id
 * @property string $name
 * @property string $price
 *
 * @property ObjectProducts[] $objectProducts
 * @property Company $company
 * @property ProductGroups $group
 * @property SaleItems[] $saleItems
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_id', 'company_id'], 'required'],
            [['company_id', 'group_id'], 'integer'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductGroups::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'group_id' => 'Group ID',
            'name' => 'Наименование',
            'nameText' => 'Наименование',
            'price' => 'Цена',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectProducts()
    {
        return $this->hasMany(ObjectProducts::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(ProductGroups::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleItems()
    {
        return $this->hasMany(SaleItems::className(), ['product_id' => 'id']);
    }

    public function getPriceByPartnerAndObject($partner_id, $object_id)
    {
        $partner = Partners::findOne($partner_id);

        if (!$partner)
            return $this->price;

        $object = Objects::findOne($object_id);

        if (!$object)
            return $this->price;

        $discount = Discounts::findOne(['partner_id' => $partner_id, 'product_id' => $this->id]);

        if (!$discount)
            return $this->price;

        $discount_object = $discount->getDiscountObjects()->where(['object_id' => $object_id])->one();

        if ($discount_object)
        {
            $price_object = $discount_object->price;

            return $price_object;
        }

        return $discount->price;
    }

    public function getPriceByCardAndObject($card_id, $object_id)
    {
        $card = Cards::findOne($card_id);

        if (!$card)
            return $this->price;

        $object = Objects::findOne($object_id);

        if (!$object)
            return $this->price;

        $discount = Discounts::findOne(['partner_id' => $card->partner_id, 'product_id' => $this->id]);

        if (!$discount)
            return $this->price;

        $discount_card = $discount->getDiscountCards()->where(['card_id' => $card_id])->one();
        $discount_object = $discount->getDiscountObjects()->where(['object_id' => $object_id])->one();

        if ($discount_object && $discount_card)
        {
            $price_card = $discount_card->price;
            $price_object = $discount_object->price;

            if ($price_card >= $price_object)
                return $price_object;
            else
                return $price_card;
        }

        return $discount->price;
    }

    public function getNameText()
    {
        $name = $this->name;

        if (!\Yii::$app->user->identity)
            return $name;

        $company = \Yii::$app->user->identity->company;
        if ($company->id != $this->company_id)
        {
            $prefix = $this->company->name;
            $name = "(".$prefix.") ".$name;
        }

        return $name;
    }
}
