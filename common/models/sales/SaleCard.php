<?php

namespace common\models\sales;

use common\models\partners\Cards;
use Yii;
use yii\base\Event;

/**
 * This is the model class for table "sale_card".
 *
 * @property int $id
 * @property int $sale_id
 * @property int $card_id
 * @property string $bonus
 * @property string $bonus_percent
 *
 * @property Cards $card
 * @property Sales $sale
 */
class SaleCard extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_card';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id', 'card_id'], 'integer'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cards::className(), 'targetAttribute' => ['card_id' => 'id']],
            [['sale_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sales::className(), 'targetAttribute' => ['sale_id' => 'id']],
            [['bonus', 'bonus_percent'], 'number']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Sale ID',
            'card_id' => 'Card ID',
            'bonus' => 'Бонус',
            'bonus_percent' => '%, бонуса'
        ];
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INSERT, [Cards::class, "updateBonusBalance"]);
        $this->on(self::EVENT_AFTER_UPDATE, [Cards::class, "updateBonusBalance"]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(Cards::className(), ['id' => 'card_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sales::className(), ['id' => 'sale_id']);
    }

    public function updateBonusFields(Event $event = null)
    {
        if ($event){
            $sale = $event->sender;
            $model = $sale->saleCard;

            if (!$model)
                return;
        }
        else
            $model = $this;

        $sale = $model->sale;

        if ($sale->type_pay == Sales::TYPE_PAY_BONUS)
        {
            $model->bonus = 0;
            $model->save();

            return;
        }

        if (!$model->bonus_percent)
            $model->bonus_percent = $model->card->bonusPercent;

        $sum_bonus = $sale->sum * ($model->bonus_percent/100);
        $model->bonus = $sum_bonus;

        $model->save();
    }
}
