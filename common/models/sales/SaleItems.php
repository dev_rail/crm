<?php

namespace common\models\sales;

use common\models\products\Products;
use Yii;

/**
 * This is the model class for table "sale_items".
 *
 * @property int $id
 * @property int $sale_id
 * @property int $product_id
 * @property string $price
 * @property string $count
 * @property string $sum
 * @property string $actual_price
 * @property string $actual_sum
 *
 * @property Products $product
 * @property Sales $sale
 */
class SaleItems extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_items';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id'], 'required'],
            [['sale_id', 'product_id'], 'integer'],
            [['price', 'count', 'sum', 'actual_price', 'actual_sum'], 'number'],
            ['sum', 'validateSale'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['sale_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sales::className(), 'targetAttribute' => ['sale_id' => 'id']]
        ];
    }

    public function init()
    {
        parent::init();

        $this->on(static::EVENT_BEFORE_UPDATE, [$this, 'updateSum']);
        $this->on(static::EVENT_BEFORE_INSERT, [$this, 'updateSum']);

        $this->on(static::EVENT_AFTER_UPDATE, [Sales::class, 'updateSum']);
        $this->on(static::EVENT_AFTER_INSERT, [Sales::class, 'updateSum']);
        $this->on(static::EVENT_AFTER_DELETE, [Sales::class, 'updateSum']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Sale ID',
            'product_id' => 'Продукт',
            'price' => 'Цена',
            'count' => 'Количество',
            'sum' => 'Сумма',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sales::className(), ['id' => 'sale_id']);
    }

    public function updateSum()
    {
        $this->sum = $this->getSum();
        $this->actual_sum = $this->getActualSum();
    }

    public function getSum()
    {
        return $this->count * $this->price;
    }

    public function getActualSum()
    {
        return $this->count * $this->actual_price;
    }


    public function validateSale()
    {
        $sale = $this->sale;

        $sale->sum += $this->getSum();

        if (!$sale->validate())
            $this->addError('sum', $sale->firstErrors);
    }

    public function getSaleItemCashBack()
    {
        return $this->hasOne(SaleItemCashBack::class, ['sale_item_id' => 'id']);
    }
}
