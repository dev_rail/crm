<?php

namespace common\models\sales;

use common\models\Company;
use common\models\partners\Partners;
use Yii;
use yii\base\Event;

/**
 * This is the model class for table "sales".
 *
 * @property int $id
 * @property int $company_id
 * @property int $partner_id
 * @property int $date
 * @property string $sum
 * @property integer $type_pay
 *
 * @property SaleCard $saleCard
 * @property SaleItems[] $saleItems
 * @property SaleObject[] $saleObjects
 * @property Company $company
 * @property Partners $partner
 */
class Sales extends \yii\db\ActiveRecord
{
    //Электронно
    const TYPE_PAY_ELECTRON = 1;

    //Наличные
    const TYPE_PAY_CASH = 2;

    //Бонус
    const TYPE_PAY_BONUS = 3;


    const TYPES_PAY = [
        self::TYPE_PAY_ELECTRON => 'Электронно',
        self::TYPE_PAY_CASH => 'Наличные',
        self::TYPE_PAY_BONUS => 'Бонус'
    ];


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sales';
    }

    public function init()
    {
        parent::init();
        $this->on(self::EVENT_AFTER_INSERT, [Partners::class, 'updateBalance']);
        $this->on(self::EVENT_AFTER_UPDATE, [Partners::class, 'updateBalance']);
        $this->on(self::EVENT_AFTER_DELETE, [Partners::class, 'updateBalance']);

        $this->on(self::EVENT_AFTER_INSERT, [SaleCard::class, 'updateBonusFields']);
        $this->on(self::EVENT_AFTER_UPDATE, [SaleCard::class, 'updateBonusFields']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'partner_id'], 'required'],
            [['company_id', 'partner_id', 'date', 'type_pay'], 'integer'],
            ['type_pay', 'default', 'value' => self::TYPE_PAY_ELECTRON],
            [['sum'], 'number'],
            ['type_pay', 'validateBonusBalance'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'partner_id' => 'Контрагент',
            'date' => 'Дата',
            'sum' => 'Сумма',
            'payText' => 'Оплата',
            'type_pay' => 'Способ оплаты'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleCard()
    {
        return $this->hasOne(SaleCard::className(), ['sale_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleItems()
    {
        return $this->hasMany(SaleItems::className(), ['sale_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleObjects()
    {
        return $this->hasMany(SaleObject::className(), ['sale_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    public function updateSum(Event $event)
    {
        $model = $event->sender;
        /** @var Sales $sale */
        $sale = $model->sale;

        $sale->sum = $sale->getSaleItems()->sum('sum');
        $sale->save();
    }

    public function getDateText()
    {
        return date("d.m.Y H:i:s", $this->date);
    }

    public function getPayText()
    {
        return self::TYPES_PAY[$this->type_pay];
    }

    public function getTypesPay()
    {
        $result = self::TYPES_PAY;
        if (!$this->saleCard)
            unset($result[self::TYPE_PAY_BONUS]);

        return $result;
    }

    public function validateBonusBalance()
    {
        if ($this->type_pay != self::TYPE_PAY_BONUS)
            return;

        if (!$this->saleCard)
            return;

        $card = $this->saleCard->card;

        if ($this->sum > $card->bonus_balance)
            $this->addError('type_pay', 'Недостаточно средств для списания с бонусного счета!');
    }
}
