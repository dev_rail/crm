<?php

namespace common\models\sales;

use common\models\CompanyUsers;
use common\models\objects\Objects;
use common\models\objects\Terminals;
use common\models\objects\TerminalSessions;
use Yii;

/**
 * This is the model class for table "sale_object".
 *
 * @property int $id
 * @property int $sale_id
 * @property int $object_id
 * @property int $terminal_id
 * @property int $user_id
 * @property int $session_id
 *
 * @property Objects $object
 * @property Sales $sale
 * @property TerminalSessions $session
 * @property Terminals $terminal
 * @property CompanyUsers $user
 */
class SaleObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_id', 'object_id', 'terminal_id', 'user_id', 'session_id'], 'integer'],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['sale_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sales::className(), 'targetAttribute' => ['sale_id' => 'id']],
            [['session_id'], 'exist', 'skipOnError' => true, 'targetClass' => TerminalSessions::className(), 'targetAttribute' => ['session_id' => 'id']],
            [['terminal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Terminals::className(), 'targetAttribute' => ['terminal_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyUsers::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_id' => 'Sale ID',
            'object_id' => 'Object ID',
            'terminal_id' => 'Terminal ID',
            'user_id' => 'User ID',
            'session_id' => 'Session ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSale()
    {
        return $this->hasOne(Sales::className(), ['id' => 'sale_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(TerminalSessions::className(), ['id' => 'session_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminal()
    {
        return $this->hasOne(Terminals::className(), ['id' => 'terminal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(CompanyUsers::className(), ['id' => 'user_id']);
    }
}
