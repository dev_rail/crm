<?php

namespace common\models\sales;

use Yii;

/**
 * This is the model class for table "sale_item_cash_back".
 *
 * @property int $id
 * @property int $sale_item_id
 * @property string $price
 * @property string $diff
 * @property string $sum
 *
 * @property SaleItems $saleItem
 */
class SaleItemCashBack extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_item_cash_back';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_item_id'], 'integer'],
            [['price', 'diff', 'sum'], 'number'],
            [['sale_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => SaleItems::className(), 'targetAttribute' => ['sale_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sale_item_id' => 'Sale Item ID',
            'price' => 'Price',
            'diff' => 'Diff',
            'sum' => 'Sum',
        ];
    }

    public function init()
    {
        parent::init();
        $this->on(static::EVENT_BEFORE_UPDATE, [$this, 'updateSum']);
        $this->on(static::EVENT_BEFORE_INSERT, [$this, 'updateSum']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleItem()
    {
        return $this->hasOne(SaleItems::className(), ['id' => 'sale_item_id']);
    }

    public function updateSum()
    {
        $this->diff = $this->price - $this->saleItem->price;
        $this->sum = $this->diff * $this->saleItem->count;
    }
}
