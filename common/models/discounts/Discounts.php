<?php

namespace common\models\discounts;

use common\components\DiscountBehavior;
use common\models\Company;
use common\models\partners\Partners;
use common\models\products\Products;
use Yii;

/**
 * This is the model class for table "discounts".
 *
 * @property int $id
 * @property int $company_id
 * @property int $partner_id
 * @property int $product_id
 * @property int $type
 * @property string $amount
 *
 * @property DiscountCards[] $discountCards
 * @property DiscountObjects[] $discountObjects
 * @property Company $company
 * @property Partners $partner
 */
class Discounts extends \yii\db\ActiveRecord
{
    const TYPE_FIX_PRICE = 1;
    const TYPE_PERCENT = 2;
    const TYPE_PRICE = 3;

    const TYPES = [
        self::TYPE_FIX_PRICE => 'Фиксированная цена',
        self::TYPE_PERCENT => 'Процент',
        self::TYPE_PRICE => 'Скидка в копейках'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'partner_id', 'type', 'product_id'], 'integer'],
            [['amount'], 'number'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => Partners::className(), 'targetAttribute' => ['partner_id' => 'id']],
            [['partner_id', 'product_id'], 'unique', 'targetAttribute' => ['partner_id', 'product_id']]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'partner_id' => 'Контрагент',
            'type' => 'Тип',
            'amount' => 'Значение',
            'product_id' => 'Продукт'
        ];
    }

    public function behaviors()
    {
        return [
            DiscountBehavior::class
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountCards()
    {
        return $this->hasMany(DiscountCards::className(), ['discount_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountObjects()
    {
        return $this->hasMany(DiscountObjects::className(), ['discount_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartner()
    {
        return $this->hasOne(Partners::className(), ['id' => 'partner_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
