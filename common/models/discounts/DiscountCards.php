<?php

namespace common\models\discounts;

use common\components\DiscountBehavior;
use common\models\partners\Cards;
use Yii;

/**
 * This is the model class for table "discount_cards".
 *
 * @property int $id
 * @property int $discount_id
 * @property int $card_id
 * @property int $type
 * @property string $amount
 *
 * @property Cards $card
 * @property Discounts $discount
 */
class DiscountCards extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discount_cards';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discount_id', 'card_id', 'type'], 'integer'],
            [['amount'], 'number'],
            [['card_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cards::className(), 'targetAttribute' => ['card_id' => 'id']],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discounts::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['discount_id', 'card_id'], 'unique', 'targetAttribute' => ['discount_id', 'card_id'], 'message' => 'Для данной скидки карта уже добавлена!']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_id' => 'Discount ID',
            'card_id' => 'Карта',
            'type' => 'Тип',
            'amount' => 'Значение',
        ];
    }

    public function behaviors()
    {
        return [
            DiscountBehavior::class
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCard()
    {
        return $this->hasOne(Cards::className(), ['id' => 'card_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discounts::className(), ['id' => 'discount_id']);
    }
}
