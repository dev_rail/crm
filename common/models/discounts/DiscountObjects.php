<?php

namespace common\models\discounts;

use common\components\DiscountBehavior;
use common\models\objects\Objects;
use Yii;

/**
 * This is the model class for table "discount_objects".
 *
 * @property int $id
 * @property int $discount_id
 * @property int $object_id
 * @property int $type
 * @property string $amount
 *
 * @property Discounts $discount
 * @property Objects $object
 */
class DiscountObjects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'discount_objects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['discount_id', 'object_id', 'type'], 'integer'],
            [['amount'], 'number'],
            [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discounts::className(), 'targetAttribute' => ['discount_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['discount_id', 'object_id'], 'unique', 'targetAttribute' => ['discount_id', 'object_id'], 'message' => 'Для данной скидки объект уже добавлен!']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discount_id' => 'Discount ID',
            'object_id' => 'Объект',
            'type' => 'Тип',
            'amount' => 'Значение',
        ];
    }

    public function behaviors()
    {
        return [
            DiscountBehavior::class
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discounts::className(), ['id' => 'discount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::className(), ['id' => 'object_id']);
    }
}
