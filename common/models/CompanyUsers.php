<?php

namespace common\models;

use common\models\objects\TerminalSessions;
use common\models\partners\UsersPartners;
use common\models\sales\SaleObject;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "company_users".
 *
 * @property int $id
 * @property int $company_id
 * @property string $username
 * @property string $password
 *
 * @property Company $company
 * @property SaleObject[] $saleObjects
 * @property TerminalSessions[] $terminalSessions
 */
class CompanyUsers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['username', 'password'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['username', 'company_id'], 'unique', 'targetAttribute' => ['username', 'company_id'], 'message' => 'Пользователь с данным логином уже существует!']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'username' => 'Логин',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleObjects()
    {
        return $this->hasMany(SaleObject::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminalSessions()
    {
        return $this->hasMany(TerminalSessions::className(), ['user_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUserPartners()
    {
        return $this->hasMany(UsersPartners::class, ['user_id' => 'id']);
    }

    public function setPassword($password)
    {
        $this->password = \Yii::$app->security->generatePasswordHash($password);
    }
}
