<?php

namespace common\models\objects;

use common\models\Company;
use common\models\sales\SaleObject;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "terminals".
 *
 * @property int $id
 * @property int $company_id
 * @property int $object_id
 * @property string $imei
 *
 * @property SaleObject[] $saleObjects
 * @property TerminalSessions[] $terminalSessions
 * @property Company $company
 * @property Objects $object
 */
class Terminals extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'terminals';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'object_id'], 'required'],
            [['company_id', 'object_id'], 'integer'],
            [['imei'], 'string', 'max' => 255],
            [['imei'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'object_id' => 'Объект',
            'imei' => 'Imei',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleObjects()
    {
        return $this->hasMany(SaleObject::className(), ['terminal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminalSessions()
    {
        return $this->hasMany(TerminalSessions::className(), ['terminal_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::className(), ['id' => 'object_id']);
    }

    public function getActiveTerminalSession()
    {
        return $this->getTerminalSessions()->where(['close' => false])->one();
    }

    public function getActiveToken()
    {
        $session = $this->activeTerminalSession;

        return ArrayHelper::getValue($session, "token", false);
    }
}
