<?php

namespace common\models\objects;

use common\models\Company;
use common\models\CompanyUsers;
use common\models\sales\SaleObject;
use Yii;

/**
 * This is the model class for table "terminal_sessions".
 *
 * @property int $id
 * @property int $company_id
 * @property int $object_id
 * @property int $terminal_id
 * @property int $user_id
 * @property int $created_at
 * @property int $closed_at
 * @property string $token
 * @property int $close
 *
 * @property SaleObject[] $saleObjects
 * @property Company $company
 * @property Objects $object
 * @property Terminals $terminal
 * @property CompanyUsers $user
 */
class TerminalSessions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'terminal_sessions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'object_id', 'terminal_id', 'user_id', 'created_at', 'closed_at', 'close'], 'integer'],
            [['token'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objects::className(), 'targetAttribute' => ['object_id' => 'id']],
            [['terminal_id'], 'exist', 'skipOnError' => true, 'targetClass' => Terminals::className(), 'targetAttribute' => ['terminal_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyUsers::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'object_id' => 'Object ID',
            'terminal_id' => 'Terminal ID',
            'user_id' => 'User ID',
            'created_at' => 'Created At',
            'closed_at' => 'Closed At',
            'token' => 'Token',
            'close' => 'Close',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleObjects()
    {
        return $this->hasMany(SaleObject::className(), ['session_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(Objects::className(), ['id' => 'object_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminal()
    {
        return $this->hasOne(Terminals::className(), ['id' => 'terminal_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(CompanyUsers::className(), ['id' => 'user_id']);
    }
}
