<?php

namespace common\models\objects;

use common\models\Company;
use common\models\discounts\DiscountObjects;
use common\models\sales\SaleObject;

/**
 * This is the model class for table "objects".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $address
 * @property string $lat
 * @property string $lng
 *
 * @property DiscountObjects[] $discountObjects
 * @property ObjectProducts[] $objectProducts
 * @property Company $company
 * @property SaleObject[] $saleObjects
 * @property TerminalSessions[] $terminalSessions
 * @property Terminals[] $terminals
 */
class Objects extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'objects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'name'], 'required'],
            [['company_id'], 'integer'],
            [['address', 'lat', 'lng'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Название',
            'nameText' => 'Название',
            'address' => 'Адрес'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountObjects()
    {
        return $this->hasMany(DiscountObjects::className(), ['object_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjectProducts()
    {
        return $this->hasMany(ObjectProducts::className(), ['object_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleObjects()
    {
        return $this->hasMany(SaleObject::className(), ['object_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminalSessions()
    {
        return $this->hasMany(TerminalSessions::className(), ['object_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTerminals()
    {
        return $this->hasMany(Terminals::className(), ['object_id' => 'id']);
    }

    public function getNameText()
    {
        $name = $this->name;

        if (!\Yii::$app->user->identity)
            return $name;

        $company = \Yii::$app->user->identity->company;
        if ($company->id != $this->company_id)
        {
            $prefix = $this->company->name;
            $name = "(".$prefix.") ".$name;
        }

        return $name;
    }
}
