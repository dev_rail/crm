<?php

namespace common\components;


use yii\base\Component;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class ArrayList extends Component
{
    public function get($model, $id, $name)
    {
        $query = $model::find();
        $result = $query->all();

        $array = ArrayHelper::map($result, $id, $name);


        return ArrayHelper::merge([null => 'Не установлено'], $array);
    }
}