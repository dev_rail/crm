<?php

namespace common\components;


use common\models\discounts\Discounts;
use yii\base\Behavior;

class DiscountBehavior extends Behavior
{
    public function getPrice()
    {
        $model = $this->owner;

        if ($model instanceof Discounts)
            $product = $model->product;
        else
            $product = $model->discount->product;

        $price = $product->price;

        if (!$model->amount)
            return $price;

        switch ($model->type) {
            case Discounts::TYPE_FIX_PRICE:
                $price = $model->amount;
                break;
            case Discounts::TYPE_PERCENT:
                $price = $price - ($price * ($model->amount/100));
                break;
            case Discounts::TYPE_PRICE:
                $price = $price - $model->amount/100;
                break;
        }

        return $price;
    }
}